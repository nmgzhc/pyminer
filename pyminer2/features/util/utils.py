"""
工具汇总。

作者：廖俊易
"""
import os
import logging
from qtpy.QtWidgets import QFileDialog

from pyminer2.features.io.settings import Settings
from pyminer2.features.io import sample


class importutils(object):
    def doSPSSImport(self):
        self.file_path, filetype = QFileDialog.getOpenFileName(self,
                                                                      '选择文件', Settings.get_instance()['work_dir'],
                                                                      "SPSS文件 (*.sav *.zsav)")  # 设置文件扩展名过滤,用双分号间隔

        if len(self.file_path) == 0:
            logging.info("\n取消选择")
            return
        else:

            if os.path.split(self.file_path)[1].endswith(('sav', 'zsav')):
                if len(self.file_path) > 0:
                    self.import_spss_form = sample.ImportSpssForm()
                    self.import_spss_form.file_path_init(self.file_path)
                    self.import_spss_form.exec_()
                else:
                    logging.info("信号发射失败")

    def doMATLABImport(self):
        self.file_path, filetype = QFileDialog.getOpenFileName(self,
                                                               '选择文件', Settings.get_instance()['work_dir'],
                                                               "MATLAB文件 (*.mat)")  # 设置文件扩展名过滤,用双分号间隔

        if len(self.file_path) == 0:
            logging.info("\n取消选择")
            return
        else:

            if os.path.split(self.file_path)[1].endswith(('mat')):
                if len(self.file_path) > 0:
                    self.import_matlab_form = sample.ImportMatlabForm()
                    self.import_matlab_form.file_path_init(self.file_path)
                    self.import_matlab_form.exec_()
                else:
                    logging.info("信号发射失败")

    def doSASImport(self):
        self.file_path, filetype = QFileDialog.getOpenFileName(self,
                                                                      '选择文件', Settings.get_instance()['work_dir'],
                                                                      "SAS文件 (*.sas7bdat)")  # 设置文件扩展名过滤,用双分号间隔

        if len(self.file_path) == 0:
            logging.info("\n取消选择")
            return
        else:

            if os.path.split(self.file_path)[1].endswith(('sas7bdat')):
                if len(self.file_path) > 0:
                    self.import_sas_form = sample.ImportSasForm()
                    self.import_sas_form.file_path_init(self.file_path)
                    self.import_sas_form.exec_()
                else:
                    logging.info("信号发射失败")

    def doExcelImport(self):
        self.file_path, filetype = QFileDialog.getOpenFileName(self,
                                                                      '选择文件', Settings.get_instance()['work_dir'],
                                                                      "EXCEL文件 (*.xls *.xlsx *.xlsm *.xltx *.xltm)")  # 设置文件扩展名过滤,用双分号间隔

        if len(self.file_path) == 0:
            logging.info("\n取消选择")
            return
        else:
            if os.path.split(self.file_path)[1].endswith(('xls', 'xlsx', 'xlsm', 'xltx', 'xltm')):
                if len(self.file_path) > 0:
                    self.import_excel_form = sample.ImportExcelForm()
                    self.import_excel_form.file_path_init(self.file_path)
                    self.import_excel_form.exec_()
                else:
                    logging.info("信号发射失败--导入文件已选择")

    def doTextImport(self):
        file_path, filetype = QFileDialog.getOpenFileName(self,
                                                                 '选择文件', Settings.get_instance()['work_dir'],
                                                                 "文本文件 (*.txt *.csv *.tsv *.tab *.dat)")
        if len(file_path) == 0:
            logging.info("\n取消选择")
            return
        else:
            self.import_form = sample.ImportForm()
            self.import_form.file_path_init(file_path)
            self.import_form.exec_()
