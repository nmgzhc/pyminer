"""
本模块定义了一些操作系统相关的工具，包括运行操作系统中的命令、运行 python 文件等。
"""


import os
import platform
import subprocess
import sys


def check_platform() -> str:
    system = platform.system()
    plat = platform.platform(1, 1)
    return system.lower()


def run_command_in_terminal(cmd: str, close_mode: str = 'wait_key'):
    """
    根据操作系统及关闭模式，运行控制台命令。

    TODO (panhaoyu) 关于各个关闭模式的作用需要进行定义。

    Args:
        cmd: 需要执行的命令。
        close_mode: 关闭的模式。
    """
    platform_name = check_platform()
    if platform_name == 'windows':
        close_action = {'auto': 'start cmd.exe /k \"%s &&exit \"',
                        'no': 'start cmd.exe /k \"%s \"',
                        'wait_key': 'start cmd.exe /k \"%s &&pause &&exit \"'
                        }
        command = close_action[close_mode] % cmd
        subprocess.Popen(command, shell=True)

    elif platform_name == 'linux':
        ret = os.system('which gnome-terminal')

        if ret == 0:
            close_action = {'auto': 'deepin-terminal -C \"%s\"',
                            'no': 'deepin-terminal -C \"%s\" --keep-open',
                            'wait_key': 'deepin-terminal -C \"%s\" --keep-open'
                            }
            command = close_action[close_mode] % cmd
            subprocess.Popen(command, shell=True)
        else:
            close_action = {'auto': 'gnome-terminal -x bash -c "%s;"',
                            'no': 'gnome-terminal -x bash -c "%s; read"',
                            'wait_key': 'gnome-terminal -x bash -c "%s; read"'
                            }
            command = close_action[close_mode] % (cmd)
            subprocess.Popen(command, shell=True)


def run_python_file_in_terminal(file_path, interpreter_path: str = None, close_mode: str = 'wait_key'):
    """
    在控制台中执行一个 ``python`` 文件。

    See Also:
        :meth:`run_command_in_terminal`

    Args:
        file_path: python文件的路径。
        interpreter_path: 解释器的路径，默认指定为运行PyMiner的解释器。
        close_mode: 关闭模式，见 :meth:`run_command_in_terminal`
    """
    if interpreter_path is None:
        interpreter_path = sys.executable
    run_command_in_terminal('%s %s' % (interpreter_path, file_path), close_mode=close_mode)


def check_application(app_name):
    """
    运行一行命令。

    TODO (panhaoyu) 未发现该函数的意义，是否可以删除？

    Args:
        app_name: 某个可执行命令。
    """
    os.system(app_name)


def get_parent_path(path, storey=1):
    """
    获取文件或文件夹的父路径。

    TODO (panhaoyu) 未发现这个函数的意义，是否可以删除？

    这个函数可以很方便地通过 ``os.path`` 模块实现。

    Args:
        path: 某个路径。
        storey: 向上查询的次数。

    Returns:
        按照给定的路径向上查询 ``storey`` 级的父路径。
    """
    for i in range(storey):
        path = os.path.dirname(path)
    return path


if __name__ == '__main__':
    plat = check_platform()
    print(plat)
