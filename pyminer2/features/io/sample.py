import os
import sys
import logging
import numpy as np
import pandas as pd
import datetime

# 导入PyQt5模块
from qtpy.QtWidgets import *
from qtpy.QtCore import *

# 导入功能组件
from pyminer2.ui.data.data_import_text import Ui_Form as Import_Ui_Form
from pyminer2.ui.data.data_import_excel import Ui_Form as ExcelImport_Ui_Form
from pyminer2.ui.data.data_import_spss import Ui_Form as SPSSImport_Ui_Form
from pyminer2.ui.data.data_import_sas import Ui_Form as SASImport_Ui_Form
from pyminer2.ui.data.data_import_matlab import Ui_Form as MATLABImport_Ui_Form
from pyminer2.ui.data.data_import_mysql import Ui_Form as ImportMysql_Ui_Form
from pyminer2.ui.data.data_import_oracle import Ui_Form as ImportOracle_Ui_Form
from pyminer2.ui.data.data_import_postgresql import Ui_Form as ImportPostgreSQL_Ui_Form
from pyminer2.features.io.settings import Settings
from pyminer2.core import *

# 定义日志输出格式
logging.basicConfig(format="%(asctime)s %(name)s:%(levelname)s:%(message)s", datefmt="%Y-%m-%d %H:%M:%S",
                    level=logging.INFO)


class ImportDialog(QDialog):
    signal_data_change = Signal(str, dict, str, str, str, str, str)  # 自定义信号，用于传递文件路径

    def openFile(self):
        pass

    def center(self):
        """
        将窗口置于中心
        :return:
        """
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def keyPressEvent(self, e):
        """
        按键盘Escape退出当前窗口
        @param e:
        """
        if e.key() == Qt.Key_Escape:
            button = QMessageBox.question(self, "Question", "是否退出当前窗口？",
                                          QMessageBox.Ok | QMessageBox.Cancel, QMessageBox.Ok)

            if button == QMessageBox.Ok:
                self.close()

    def import_send_dataset(self):
        """
        [TODO（侯展意）]增加数据导入的时间、metadata等。这一部分还需要再添加！
        [TODO]增加数据集名称判断的逻辑！
        这个方法与具体导入sas，spss还是excel数据都是无关的。
        其实意思就是把pandas数据加入到工作空间中。
        :return:
        """
        from pyminer2.workspace.datamanager.datamanager import data_manager
        logging.info("发射导入数据信号")
        if len(self.current_dataset) > 0:
            e = os.path.splitext(os.path.basename(self.file_path))
            if len(e) > 0:
                var_name = e[0]
            else:
                logging.info('文件路径%s无效' % self.file_path)
                return
            while (1):
                name, ok = QInputDialog.getText(self, "变量名", "输入新的变量名称:", QLineEdit.Normal, var_name)
                if ok and (len(name) != 0):
                    if name in data_manager.varset.keys():
                        QMessageBox.warning(self, "提示", "变量名已存在")
                        continue
                    elif not name.isidentifier():
                        QMessageBox.warning(self, '提示', '变量名无效\n提示：\n1、不要以数字开头;\n2、不要包含除下划线外的所有符号。')
                        continue
                    else:
                        var_name = name
                        break
                else:
                    self.close()

            data_manager.set_var(var_name, self.current_dataset)  # 将数据导入工作空间
            logging.info("导入数据信号已发射")
            self.close()

        else:
            logging.info("导入数据信号发射失败")
            self.close()


class ImportForm(ImportDialog, Import_Ui_Form):
    """
    "导入"窗口
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.center()

        # #QssTools.set_qss_to_obj(ui_dir + "/source/qss/patata.qss", self)
        #
        # self.file_path = ''
        # self.current_dataset_name = ""
        # self.current_dataset = pd.DataFrame()
        #
        # # self.import_file_path_init()
        #
        # # 导入窗口的相关事件
        # 在"导入"窗口，打开选择文件
        self.pushButton_choosefile.clicked.connect(self.openFile)
        # 展示数据
        self.checkBox_ifColumns.stateChanged.connect(self.import_dateset_reload)
        self.comboBox_separator.currentTextChanged.connect(self.import_dateset_reload)
        self.comboBox_encode.currentTextChanged.connect(self.import_dateset_reload)
        self.lineEdit_filePath.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_passHead.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_datasetName.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_limitRow.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_missValue.textChanged.connect(self.import_dateset_reload)

        # 更新数据
        self.pushButton_ok.clicked.connect(self.import_send_dataset)
        self.pushButton_cancel.clicked.connect(self.close)
        # 帮助
        # self.pushButton_help.clicked.connect(self.accept_signal)

    def file_path_init(self, file_path: str):
        print("开始更新数据")
        print("开始修改文件路径")
        self.file_path = file_path
        if len(self.file_path) != 0:
            self.lineEdit_filePath.setText(self.file_path)

            self.current_dataset_name = os.path.split(self.lineEdit_filePath.text())[1]
            self.lineEdit_datasetName.setText(self.current_dataset_name)
            logging.info(
                "加载成功file_path{}，datasetName：{}".format(self.file_path, self.current_dataset_name))

            if len(self.file_path) > 0:
                if self.checkBox_ifColumns.isChecked():
                    header = 0
                else:
                    header = None
                # 仅预览前100条数据
                if self.lineEdit_limitRow.text() == "全部":
                    nrows_preview = 100
                elif int(self.lineEdit_limitRow.text()) <= 100:
                    nrows_preview = int(self.lineEdit_limitRow.text())
                else:
                    nrows_preview = 100

                if self.lineEdit_limitRow.text() == "全部":
                    nrows = 100000000
                else:
                    nrows = int(self.lineEdit_limitRow.text())

                encoding = self.comboBox_encode.currentText()
                skiprows = int(self.lineEdit_passHead.text())
                sep = self.comboBox_separator.currentText()

                if self.lineEdit_missValue.text() != "默认":
                    na_values = self.lineEdit_missValue.text()
                else:
                    na_values = None

                logging.info("file_path：{}，header：{}，skiprows：{}，nrows：{}，na_values:{}".format(
                    self.file_path, header, skiprows, nrows_preview, na_values))

                self.current_dataset = pd.read_csv(self.file_path, engine="python", sep=sep,
                                                   encoding=encoding,
                                                   header=header,
                                                   skiprows=skiprows, nrows=nrows, na_values=na_values)

                if len(self.current_dataset) == 0:
                    self.tableWidget_previewData.clear()
                    logging.info("当前有效数据为空")
                else:
                    self.import_dataset_preview()
                    logging.info("数据导入成功")
            else:
                logging.info("请选择数据集")

    # def import_send_dataset(self):
    #     """
    #     [TODO（侯展意）]增加数据导入的时间、metadata等。这一部分还需要再添加！
    #     :return:
    #     """
    #     from pyminer2.workspace.datamanager.datamanager import data_manager
    #     logging.info("发射导入数据信号")
    #     if len(self.current_dataset) > 0:
    #         # create_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # 数据创建时间
    #         # update_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # 数据更新时间
    #         # path = self.file_path
    #         # print("path:", path)
    #         # file_size=str(os.path.getsize(path))
    #         # print("file_size:",file_size)
    #         # remarks = ''
    #         # self.signal_data_change.emit(self.current_dataset_name, self.current_dataset.to_dict(), path,
    #         #                              create_time, update_time, remarks, file_size)  # 发射信号
    #         e = os.path.splitext(os.path.basename(self.file_path))
    #         if len(e) >= 1:
    #             var_name = e[0]
    #             data_manager.set_var(var_name, self.current_dataset)  # 将数据导入工作空间
    #             logging.info("导入数据信号已发射")
    #         else:
    #             logging.info('文件名无效')
    #         self.close()
    #     else:
    #         logging.info("导入数据信号发射失败")
    #         self.close()

    def openFile(self):
        """
        选择文件
        """
        openfile_name = QFileDialog.getOpenFileName(self, '选择文件', Settings.get_instance()['work_dir'],
                                                    '文本文件(*.csv *.txt *.tsv)')
        self.file_path = openfile_name[0]
        self.lineEdit_filePath.setText(self.file_path)

        self.current_dataset_name = os.path.split(self.lineEdit_filePath.text())[1]
        self.lineEdit_datasetName.setText(self.current_dataset_name)

    def import_dateset_reload(self):
        """
        刷新导入的数据
        """
        header = 0
        nrows_preview = 100
        sep = ','
        skiprows = 0

        if len(self.file_path) > 0:
            if self.checkBox_ifColumns.isChecked():
                header = 'infer'
            else:
                header = None
            # 仅预览前100条数据
            if self.lineEdit_limitRow.text() == "全部":
                nrows_preview = 100
            elif int(self.lineEdit_limitRow.text()) <= 100:
                nrows_preview = int(self.lineEdit_limitRow.text())
            else:
                nrows_preview = 100

            if self.lineEdit_limitRow.text() == "全部":
                nrows = 100000000
            else:
                nrows = int(self.lineEdit_limitRow.text())

            encoding = self.comboBox_encode.currentText()
            sep = self.comboBox_separator.currentText()
            skiprows = int(self.lineEdit_passHead.text())

            if self.lineEdit_limitRow.text() != "默认":
                na_values = self.lineEdit_missValue.text()

            self.current_dataset = pd.read_csv(self.file_path, engine="python", sep=sep, encoding=encoding,
                                               header=header,
                                               skiprows=skiprows, nrows=nrows, na_values=na_values)

            if len(self.current_dataset) == 0:
                self.tableWidget_previewData.clear()
                logging.info("当前有效数据为空")
            else:
                self.import_dataset_preview()
                logging.info("数据导入成功")

    def import_dataset_preview(self):
        """
        刷新预览数据
        """
        if len(self.current_dataset) > 0:
            input_table_rows = self.current_dataset.head(100).shape[0]
            input_table_colunms = self.current_dataset.shape[1]
            input_table_header = self.current_dataset.columns.values.tolist()
            self.tableWidget_previewData.setColumnCount(input_table_colunms)
            self.tableWidget_previewData.setRowCount(input_table_rows)

            # 设置数据预览窗口的标题行
            table_header = []
            i = 1
            while i <= len(self.current_dataset.columns):
                table_header.append("C" + str(i))
                i += 1

            if self.checkBox_ifColumns.isChecked():
                self.tableWidget_previewData.setHorizontalHeaderLabels(input_table_header)
            else:
                self.tableWidget_previewData.setHorizontalHeaderLabels(table_header)
                self.current_dataset.columns = table_header
            # 数据预览窗口
            for i in range(input_table_rows):
                input_table_rows_values = self.current_dataset.iloc[[i]]
                input_table_rows_values_array = np.array(input_table_rows_values)
                input_table_rows_values_list = input_table_rows_values_array.tolist()[0]
                for j in range(input_table_colunms):
                    input_table_items_list = input_table_rows_values_list[j]

                    input_table_items = str(input_table_items_list)
                    newItem = QTableWidgetItem(input_table_items)
                    newItem.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                    self.tableWidget_previewData.setItem(i, j, newItem)


class ImportMysql(ImportDialog, ImportMysql_Ui_Form):
    """
    "导入"窗口
    """

    def __init__(self, parent=None):
        super().__init__(parent)

        self.setupUi(self)
        self.center()
        self.current_dataset = ''
        self.label_test.setHidden(True)

        # 事件
        self.pushButton_cancel.clicked.connect(self.close)
        self.pushButton_test.clicked.connect(self.database_test)
        self.pushButton_ok.clicked.connect(self.database_connect)

    def database_test(self) -> None:
        """
        检查数据库连接是否有效
        Returns:

        """
        import pymysql
        host = self.lineEdit_host.text()
        user = self.lineEdit_user.text()
        passwd = self.lineEdit_passwd.text()
        db = self.lineEdit_db.text()
        port = self.spinBox_port.value()
        charset = 'utf8'
        table = self.lineEdit_table.text()
        sql = 'select * from ' + db + '.' + table;
        print(sql)
        if len(db) == 0:
            QMessageBox.information(self, '注意', '数据库名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        elif len(table) == 0:
            QMessageBox.information(self, '注意', '表名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        else:
            try:
                conn = pymysql.connect(host=host, user=user, passwd=passwd, db=db, port=port, charset=charset)
                cur = conn.cursor()
                cur.execute(sql)
                conn.close()
                print('连接成功')
                self.label_test.setHidden(False)
                self.label_test.setText('连接成功')
                self.label_test.setStyleSheet('color: blue;')

            except Exception as Error:
                print('连接失败:' + str(Error))
                self.label_test.setHidden(False)
                self.label_test.setText('连接失败:' + str(Error))
                self.label_test.setStyleSheet('color: rgb(255, 0, 0);')

    def database_connect(self) -> None:
        """
        连接mysql数据库
        Returns:

        """
        import pymysql
        host = self.lineEdit_host.text()
        user = self.lineEdit_user.text()
        passwd = self.lineEdit_passwd.text()
        db = self.lineEdit_db.text()
        port = self.spinBox_port.value()
        charset = 'utf8'
        table = self.lineEdit_table.text()
        self.current_dataset_name = table
        self.file_path = db + '.' + table
        sql = 'select * from ' + self.file_path;
        print(sql)
        if len(db) == 0:
            QMessageBox.information(self, '注意', '数据库名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        elif len(table) == 0:
            QMessageBox.information(self, '注意', '表名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        else:
            try:
                conn = pymysql.connect(host=host, user=user, passwd=passwd, db=db, port=port, charset=charset)
                self.current_dataset = pd.read_sql(sql, con=conn)
                self.import_send_dataset(table)  # 更新当前数据
                conn.close()
                print('连接成功')
                self.label_test.setHidden(False)
                self.label_test.setText('连接成功')
                self.label_test.setStyleSheet('color: blue;')

            except Exception as Error:
                print('连接失败:' + str(Error))
                self.label_test.setHidden(False)
                self.label_test.setText('连接失败:' + str(Error))
                self.label_test.setStyleSheet('color: rgb(255, 0, 0);')

    def import_send_dataset(self, var_name: str) -> None:
        """
        [TODO（侯展意）]增加数据导入的时间、metadata等。这一部分还需要再添加！
        这个方法与具体导入sas，spss还是excel数据都是无关的。
        其实意思就是把pandas数据加入到工作空间中。
        :return:
        """
        from pyminer2.workspace.datamanager.datamanager import data_manager
        logging.info("发射导入数据信号")
        if len(self.current_dataset) > 0:
            # create_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # 数据创建时间
            # update_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # 数据更新时间
            # path = self.file_path
            # print("path:", path)
            # file_size=str(os.path.getsize(path))
            # print("file_size:",file_size)
            # remarks = ''
            # self.signal_data_change.emit(self.current_dataset_name, self.current_dataset.to_dict(), path,
            #                              create_time, update_time, remarks, file_size)  # 发射信号

            if var_name in data_manager.varset.keys():  # 判断变量是否已经存在。如果存在，那么导入需要用户确认。

                # 创建一个问答框，注意是Question
                dialog = QMessageBox(QMessageBox.Question, self.tr('变量已存在'),
                                     self.tr('工作空间已存在变量  \"%s\",\n'
                                             '是否覆盖?' % var_name))

                # 添加按钮，可用中文
                yes = dialog.addButton(self.tr('覆盖'), QMessageBox.YesRole)
                new = dialog.addButton(self.tr('重命名'), QMessageBox.ActionRole)
                no = dialog.addButton(self.tr('取消'), QMessageBox.NoRole)

                # 设置消息框的位置，大小无法设置
                dialog.setGeometry(500, 500, 0, 0)

                # 显示该问答框
                dialog.exec_()

                if dialog.clickedButton() == new:
                    name, ok = QInputDialog.getText(self, "重命名", "输入新的变量名称:", QLineEdit.Normal, "")
                    if ok and (len(name) != 0):
                        if name in data_manager.varset.keys():
                            QMessageBox.warning(self, "提示", "变量名已存在", )
                        else:
                            var_name = name

                elif dialog.clickedButton() == no:
                    self.close()
                    return
            data_manager.set_var(var_name, self.current_dataset)  # 将数据导入工作空间
            logging.info("导入数据信号已发射")
            self.close()

        else:
            logging.info("导入数据信号发射失败")
            self.close()


class ImportOracle(ImportDialog, ImportOracle_Ui_Form):
    """
    "导入"窗口
    """

    def __init__(self, parent=None):
        super().__init__(parent)

        self.setupUi(self)
        self.center()
        self.current_dataset = ''
        self.label_test.setHidden(True)

        # 事件
        self.pushButton_cancel.clicked.connect(self.close)
        self.pushButton_test.clicked.connect(self.database_test)
        self.pushButton_ok.clicked.connect(self.database_connect)

    def database_test(self) -> None:
        """
        检查数据库连接是否有效
        Returns:

        """
        from pyminer2.core.io.oracle import cx_Oracle
        host = self.lineEdit_host.text()
        user = self.lineEdit_user.text()
        passwd = self.lineEdit_passwd.text()
        db = self.lineEdit_db.text()
        port = self.spinBox_port.value()
        charset = 'utf8'
        table = self.lineEdit_table.text()
        sql = 'select * from ' + table;
        print(sql)
        if len(db) == 0:
            QMessageBox.information(self, '注意', '数据库名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        elif len(table) == 0:
            QMessageBox.information(self, '注意', '表名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        else:
            try:
                conn = cx_Oracle.connect(user, passwd,host+'/'+db)
                cur = conn.cursor()
                cur.execute(sql)
                conn.close()
                print('连接成功')
                self.label_test.setHidden(False)
                self.label_test.setText('连接成功')
                self.label_test.setStyleSheet('color: blue;')

            except Exception as Error:
                print('连接失败:' + str(Error))
                self.label_test.setHidden(False)
                self.label_test.setText('连接失败:' + str(Error))
                self.label_test.setStyleSheet('color: rgb(255, 0, 0);')

    def database_connect(self) -> None:
        """
        连接Oracle数据库
        Returns:

        """
        from pyminer2.core.io.oracle import cx_Oracle
        host = self.lineEdit_host.text()
        user = self.lineEdit_user.text()
        passwd = self.lineEdit_passwd.text()
        db = self.lineEdit_db.text()
        port = self.spinBox_port.value()
        charset = 'utf8'
        table = self.lineEdit_table.text()
        self.current_dataset_name = table
        self.file_path = db + '.' + table
        sql = 'select * from ' + table;
        print(sql)
        if len(db) == 0:
            QMessageBox.information(self, '注意', '数据库名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        elif len(table) == 0:
            QMessageBox.information(self, '注意', '表名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        else:
            try:
                conn = cx_Oracle.connect(user, passwd,host+'/'+db)
                self.current_dataset = pd.read_sql(sql, con=conn)
                self.import_send_dataset(table)  # 更新当前数据
                conn.close()
                print('连接成功')
                self.label_test.setHidden(False)
                self.label_test.setText('连接成功')
                self.label_test.setStyleSheet('color: blue;')

            except Exception as Error:
                print('连接失败:' + str(Error))
                self.label_test.setHidden(False)
                self.label_test.setText('连接失败:' + str(Error))
                self.label_test.setStyleSheet('color: rgb(255, 0, 0);')

    def import_send_dataset(self, var_name: str) -> None:
        """
        [TODO（侯展意）]增加数据导入的时间、metadata等。这一部分还需要再添加！
        这个方法与具体导入sas，spss还是excel数据都是无关的。
        其实意思就是把pandas数据加入到工作空间中。
        :return:
        """
        from pyminer2.workspace.datamanager.datamanager import data_manager
        logging.info("发射导入数据信号")
        if len(self.current_dataset) > 0:
            # create_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # 数据创建时间
            # update_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # 数据更新时间
            # path = self.file_path
            # print("path:", path)
            # file_size=str(os.path.getsize(path))
            # print("file_size:",file_size)
            # remarks = ''
            # self.signal_data_change.emit(self.current_dataset_name, self.current_dataset.to_dict(), path,
            #                              create_time, update_time, remarks, file_size)  # 发射信号

            if var_name in data_manager.varset.keys():  # 判断变量是否已经存在。如果存在，那么导入需要用户确认。

                # 创建一个问答框，注意是Question
                dialog = QMessageBox(QMessageBox.Question, self.tr('变量已存在'),
                                     self.tr('工作空间已存在变量  \"%s\",\n'
                                             '是否覆盖?' % var_name))

                # 添加按钮，可用中文
                yes = dialog.addButton(self.tr('覆盖'), QMessageBox.YesRole)
                new = dialog.addButton(self.tr('重命名'), QMessageBox.ActionRole)
                no = dialog.addButton(self.tr('取消'), QMessageBox.NoRole)

                # 设置消息框的位置，大小无法设置
                dialog.setGeometry(500, 500, 0, 0)

                # 显示该问答框
                dialog.exec_()

                if dialog.clickedButton() == new:
                    name, ok = QInputDialog.getText(self, "重命名", "输入新的变量名称:", QLineEdit.Normal, "")
                    if ok and (len(name) != 0):
                        if name in data_manager.varset.keys():
                            QMessageBox.warning(self, "提示", "变量名已存在", )
                        else:
                            var_name = name

                elif dialog.clickedButton() == no:
                    self.close()
                    return
            data_manager.set_var(var_name, self.current_dataset)  # 将数据导入工作空间
            logging.info("导入数据信号已发射")
            self.close()

        else:
            logging.info("导入数据信号发射失败")
            self.close()


class ImportPostgreSQL(ImportDialog, ImportPostgreSQL_Ui_Form):
    """
    "导入"窗口
    """

    def __init__(self, parent=None):
        super().__init__(parent)

        self.setupUi(self)
        self.center()
        self.current_dataset = ''
        self.label_test.setHidden(True)

        # 事件
        self.pushButton_cancel.clicked.connect(self.close)
        self.pushButton_test.clicked.connect(self.database_test)
        self.pushButton_ok.clicked.connect(self.database_connect)

    def database_test(self) -> None:
        """
        检查数据库连接是否有效
        Returns:

        """
        from pyminer2.core.io.postgresql import psycopg2
        host = self.lineEdit_host.text()
        user = self.lineEdit_user.text()
        passwd = self.lineEdit_passwd.text()
        db = self.lineEdit_db.text()
        port = self.spinBox_port.value()
        charset = 'utf8'
        table = self.lineEdit_table.text()
        sql = 'select * from ' + table;
        print(sql)
        if len(db) == 0:
            QMessageBox.information(self, '注意', '数据库名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        elif len(table) == 0:
            QMessageBox.information(self, '注意', '表名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        else:
            try:
                conn = psycopg2.connect(database=db, user=user, password=passwd, host=host, port=port)
                cur = conn.cursor()
                cur.execute(sql)
                conn.close()
                print('连接成功')
                self.label_test.setHidden(False)
                self.label_test.setText('连接成功')
                self.label_test.setStyleSheet('color: blue;')

            except Exception as Error:
                print('连接失败:' + str(Error))
                self.label_test.setHidden(False)
                self.label_test.setText('连接失败:' + str(Error))
                self.label_test.setStyleSheet('color: rgb(255, 0, 0);')

    def database_connect(self) -> None:
        """
        连接Oracle数据库
        Returns:

        """
        from pyminer2.core.io.postgresql import psycopg2
        host = self.lineEdit_host.text()
        user = self.lineEdit_user.text()
        passwd = self.lineEdit_passwd.text()
        db = self.lineEdit_db.text()
        port = self.spinBox_port.value()
        charset = 'utf8'
        table = self.lineEdit_table.text()
        self.current_dataset_name = table
        self.file_path = db + '.' + table
        sql = 'select * from ' + table;
        print(sql)
        if len(db) == 0:
            QMessageBox.information(self, '注意', '数据库名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        elif len(table) == 0:
            QMessageBox.information(self, '注意', '表名不能为空', QMessageBox.Ok, QMessageBox.Ok)
        else:
            try:
                conn = psycopg2.connect(database=db, user=user, password=passwd, host=host, port=port)
                self.current_dataset = pd.read_sql(sql, con=conn)
                self.import_send_dataset(table)  # 更新当前数据
                conn.close()
                print('连接成功')
                self.label_test.setHidden(False)
                self.label_test.setText('连接成功')
                self.label_test.setStyleSheet('color: blue;')

            except Exception as Error:
                print('连接失败:' + str(Error))
                self.label_test.setHidden(False)
                self.label_test.setText('连接失败:' + str(Error))
                self.label_test.setStyleSheet('color: rgb(255, 0, 0);')

    def import_send_dataset(self, var_name: str) -> None:
        """
        [TODO（侯展意）]增加数据导入的时间、metadata等。这一部分还需要再添加！
        这个方法与具体导入sas，spss还是excel数据都是无关的。
        其实意思就是把pandas数据加入到工作空间中。
        :return:
        """
        from pyminer2.workspace.datamanager.datamanager import data_manager
        logging.info("发射导入数据信号")
        if len(self.current_dataset) > 0:
            # create_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # 数据创建时间
            # update_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # 数据更新时间
            # path = self.file_path
            # print("path:", path)
            # file_size=str(os.path.getsize(path))
            # print("file_size:",file_size)
            # remarks = ''
            # self.signal_data_change.emit(self.current_dataset_name, self.current_dataset.to_dict(), path,
            #                              create_time, update_time, remarks, file_size)  # 发射信号

            if var_name in data_manager.varset.keys():  # 判断变量是否已经存在。如果存在，那么导入需要用户确认。

                # 创建一个问答框，注意是Question
                dialog = QMessageBox(QMessageBox.Question, self.tr('变量已存在'),
                                     self.tr('工作空间已存在变量  \"%s\",\n'
                                             '是否覆盖?' % var_name))

                # 添加按钮，可用中文
                yes = dialog.addButton(self.tr('覆盖'), QMessageBox.YesRole)
                new = dialog.addButton(self.tr('重命名'), QMessageBox.ActionRole)
                no = dialog.addButton(self.tr('取消'), QMessageBox.NoRole)

                # 设置消息框的位置，大小无法设置
                dialog.setGeometry(500, 500, 0, 0)

                # 显示该问答框
                dialog.exec_()

                if dialog.clickedButton() == new:
                    name, ok = QInputDialog.getText(self, "重命名", "输入新的变量名称:", QLineEdit.Normal, "")
                    if ok and (len(name) != 0):
                        if name in data_manager.varset.keys():
                            QMessageBox.warning(self, "提示", "变量名已存在", )
                        else:
                            var_name = name

                elif dialog.clickedButton() == no:
                    self.close()
                    return
            data_manager.set_var(var_name, self.current_dataset)  # 将数据导入工作空间
            logging.info("导入数据信号已发射")
            self.close()

        else:
            logging.info("导入数据信号发射失败")
            self.close()


class ImportExcelForm(ImportDialog, ExcelImport_Ui_Form):
    """
    打开"从excel导入"窗口
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.center()

        self.file_path = ''  # 文件路径
        self.current_dataset_name = ""  # 当前数据集名称
        self.current_dataset = pd.DataFrame()  # 当前数据集

        # 导入窗口的相关事件
        # 在"导入"窗口，打开选择文件
        self.pushButton_choosefile.clicked.connect(self.openFile)
        # # 展示数据
        self.checkBox_ifColumns.stateChanged.connect(self.import_dateset_reload)
        self.comboBox_sheet.currentTextChanged.connect(self.import_dateset_reload)
        self.comboBox_encode.currentTextChanged.connect(self.import_dateset_reload)
        self.lineEdit_filePath.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_passHead.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_datasetName.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_limitRow.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_missValue.textChanged.connect(self.import_dateset_reload)
        #
        # 更新数据
        self.pushButton_ok.clicked.connect(self.import_send_dataset)
        self.pushButton_cancel.clicked.connect(self.close)

    def file_path_init(self, file_path):
        """
        #初始化excel文件路径、sheet名单,以便指定需要导入的sheet
        """
        self.file_path = file_path
        print("self.file_path:", self.file_path)
        if len(self.file_path) != 0:
            self.lineEdit_filePath.setText(self.file_path)

            import xlrd
            wb = xlrd.open_workbook(self.file_path)

            # 获取excel 工作簿中所有的sheet
            sheets = wb.sheet_names()
            self.comboBox_sheet.clear()
            for s in sheets:
                self.comboBox_sheet.addItem(s)

            self.current_dataset_name = os.path.split(self.lineEdit_filePath.text())[1]
            self.lineEdit_datasetName.setText(self.current_dataset_name)
            logging.info(
                "加载成功file_path{}，datasetName：{}".format(self.file_path, self.current_dataset_name))

            if len(self.file_path) > 0:
                if self.checkBox_ifColumns.isChecked():
                    header = 0
                else:
                    header = None
                # 仅预览前100条数据
                if self.lineEdit_limitRow.text() == "全部":
                    nrows_preview = 100
                elif int(self.lineEdit_limitRow.text()) <= 100:
                    nrows_preview = int(self.lineEdit_limitRow.text())
                else:
                    nrows_preview = 100

                if self.lineEdit_limitRow.text() == "全部":
                    nrows = 100000000
                else:
                    nrows = int(self.lineEdit_limitRow.text())

                skiprows = int(self.lineEdit_passHead.text())
                sheet = self.comboBox_sheet.currentText()

                if self.lineEdit_missValue.text() != "默认":
                    na_values = self.lineEdit_missValue.text()
                else:
                    na_values = None

                logging.info("file_path：{}，sheet_name：{}，header：{}，skiprows：{}，nrows：{}，na_values:{}".format(
                    self.file_path, sheet, header, skiprows, nrows_preview, na_values))

                self.current_dataset = pd.read_excel(self.file_path, sheet_name=sheet,
                                                     header=header,
                                                     skiprows=skiprows, nrows=nrows, na_values=na_values)

                if len(self.current_dataset) == 0:
                    self.tableWidget_previewData.clear()
                    logging.info("当前sheet页有效数据为空")
                else:
                    self.import_dateset_preview()
                    logging.info("数据导入成功")
            else:
                logging.info("请选择数据集")

    def openFile(self):
        """
        选择文件
        """
        print(Settings.get_instance()['work_dir'], Settings.get_instance())
        openfile_name = QFileDialog.getOpenFileName(self, '选择文件', Settings.get_instance()['work_dir'],
                                                    'EXCEL文件(*.xls *.xlsx *.xlsm *.xltx *.xltm)')
        logging.info(openfile_name)

        self.file_path = openfile_name[0]
        self.lineEdit_filePath.setText(self.file_path)

        # 获取excel 工作簿中所有的sheet
        import openpyxl
        wb = openpyxl.load_workbook(self.file_path)
        sheets = wb.sheetnames
        self.comboBox_sheet.clear()
        for s in sheets:
            self.comboBox_sheet.addItem(s)

        self.current_dataset_name = os.path.split(self.lineEdit_filePath.text())[1]
        self.lineEdit_datasetName.setText(self.current_dataset_name)
        logging.info("加载成功file_path{}，datasetName：{}".format(openfile_name, self.current_dataset_name))
        self.import_dateset_reload()

    def import_dateset_reload(self):
        """
        刷新导入的数据
        """
        header = 0
        nrows_preview = 100
        sep = ','
        skiprows = 0
        # datasetName 为当前已选文件对应的excel数据路径

        if len(self.file_path) > 0:
            if self.checkBox_ifColumns.isChecked():
                header = 0
            else:
                header = None
            # 仅预览前100条数据
            if self.lineEdit_limitRow.text() == "全部":
                nrows_preview = 100
            elif int(self.lineEdit_limitRow.text()) <= 100:
                nrows_preview = int(self.lineEdit_limitRow.text())
            else:
                nrows_preview = 100

            if self.lineEdit_limitRow.text() == "全部":
                nrows = 100000000
            else:
                nrows = int(self.lineEdit_limitRow.text())

            encoding = self.comboBox_encode.currentText()
            skiprows = int(self.lineEdit_passHead.text())
            sheet = self.comboBox_sheet.currentText()

            if self.lineEdit_missValue.text() != "默认":
                na_values = self.lineEdit_missValue.text()
            else:
                na_values = None

            logging.info("file_path：{}，sheet_name：{}，header：{}，skiprows：{}，nrows：{}，na_values:{}".format(
                self.file_path, sheet, header, skiprows, nrows_preview, na_values))
            if len(sheet) > 0:
                self.current_dataset = pd.read_excel(self.file_path, sheet_name=sheet,
                                                     header=header,
                                                     skiprows=skiprows, nrows=nrows, na_values=na_values)

                if len(self.current_dataset) == 0:
                    self.tableWidget_previewData.clear()
                    logging.info("当前sheet页有效数据为空")
                else:
                    self.import_dateset_preview()
                    logging.info("数据导入成功")

    def import_dateset_preview(self):
        """
        刷新预览数据
        """
        if len(self.current_dataset) > 0:
            input_table_rows = self.current_dataset.head(100).shape[0]  # 预览前100行
            input_table_colunms = self.current_dataset.shape[1]
            input_table_header = self.current_dataset.columns.values.tolist()
            self.tableWidget_previewData.setColumnCount(input_table_colunms)
            self.tableWidget_previewData.setRowCount(input_table_rows)

            # 设置数据预览窗口的标题行
            table_header = []
            i = 1
            while i <= len(self.current_dataset.columns):
                table_header.append("C" + str(i))
                i += 1

            if self.checkBox_ifColumns.isChecked():
                self.tableWidget_previewData.setHorizontalHeaderLabels(input_table_header)
            else:
                self.tableWidget_previewData.setHorizontalHeaderLabels(table_header)
                self.current_dataset.columns = table_header
            # 数据预览窗口
            for i in range(input_table_rows):
                input_table_rows_values = self.current_dataset.iloc[[i]]
                input_table_rows_values_array = np.array(input_table_rows_values)
                input_table_rows_values_list = input_table_rows_values_array.tolist()[0]
                for j in range(input_table_colunms):
                    input_table_items_list = input_table_rows_values_list[j]

                    input_table_items = str(input_table_items_list)
                    newItem = QTableWidgetItem(input_table_items)
                    newItem.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                    self.tableWidget_previewData.setItem(i, j, newItem)


class ImportSpssForm(ImportDialog, SPSSImport_Ui_Form):
    """
    打开"从spss导入"窗口
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.center()

        # QssTools.set_qss_to_obj(ui_dir + "/source/qss/patata.qss", self)

        self.file_path = ''
        self.current_dataset_name = ""
        self.current_dataset = pd.DataFrame()

        self.all_dataset = dict()

        # 导入窗口的相关事件
        # 在"导入"窗口，打开选择文件
        self.pushButton_choosefile.clicked.connect(self.openFile)
        # 展示数据
        self.checkBox_ifColumns.stateChanged['int'].connect(self.import_dateset_reload)
        self.comboBox_encode.currentTextChanged['QString'].connect(self.import_dateset_reload)
        self.lineEdit_filePath.textChanged['QString'].connect(self.import_dateset_reload)
        self.lineEdit_passHead.textChanged['QString'].connect(self.import_dateset_reload)
        self.lineEdit_datasetName.textChanged['QString'].connect(self.import_dateset_reload)
        self.lineEdit_limitRow.textChanged['QString'].connect(self.import_dateset_reload)

        # 更新数据
        self.pushButton_ok.clicked.connect(self.import_send_dataset)
        self.pushButton_cancel.clicked.connect(self.close)

    def file_path_init(self, file_path):
        """
        #初始化spss文件路径
        """
        self.file_path = file_path
        if len(self.file_path) != 0:
            self.lineEdit_filePath.setText(self.file_path)
            self.current_dataset_name = os.path.split(self.lineEdit_filePath.text())[1]
            self.lineEdit_datasetName.setText(self.current_dataset_name)
            logging.info(
                "加载成功file_path{}，datasetName：{}".format(self.file_path, self.current_dataset_name))

            if len(self.file_path) > 0:
                if self.checkBox_ifColumns.isChecked():
                    header = 0
                else:
                    header = None
                # 仅预览前100条数据
                if self.lineEdit_limitRow.text() == "全部":
                    nrows_preview = 100
                elif int(self.lineEdit_limitRow.text()) <= 100:
                    nrows_preview = int(self.lineEdit_limitRow.text())
                else:
                    nrows_preview = 100

                if self.lineEdit_limitRow.text() == "全部":
                    nrows = 100000000
                else:
                    nrows = int(self.lineEdit_limitRow.text())

                encoding = self.comboBox_encode.currentText()
                skiprows = int(self.lineEdit_passHead.text())

                logging.info("file_path：{}，header：{}，skiprows：{}，nrows：{}".format(
                    self.file_path, header, skiprows, nrows_preview))

                self.current_dataset = pd.read_spss(self.file_path)

                if len(self.current_dataset) == 0:
                    self.tableWidget_previewData.clear()
                    logging.info("当前有效数据为空")
                else:
                    self.import_dateset_preview()
                    logging.info("数据导入成功")
            else:
                print("请选择数据集")

    def openFile(self):
        """
        选择文件
        """
        openfile_name = QFileDialog.getOpenFileName(self, '选择文件', Settings.get_instance()['work_dir'], 'SPSS文件(*.sav)')
        logging.info(openfile_name)

        self.file_path = openfile_name[0]
        self.lineEdit_filePath.setText(self.file_path)

        self.current_dataset_name = os.path.split(self.lineEdit_filePath.text())[1]
        self.lineEdit_datasetName.setText(self.current_dataset_name)
        logging.info("加载成功file_path{}，datasetName：{}".format(openfile_name, self.current_dataset_name))
        self.import_dateset_reload()

    def import_dateset_reload(self):
        """
        刷新导入的数据
        """
        header = 0
        nrows_preview = 100
        sep = ','
        skiprows = 0

        if len(self.file_path) > 0:
            if self.checkBox_ifColumns.isChecked():
                header = 0
            else:
                header = None
            # 仅预览前100条数据
            if self.lineEdit_limitRow.text() == "全部":
                nrows_preview = 100
            elif int(self.lineEdit_limitRow.text()) <= 100:
                nrows_preview = int(self.lineEdit_limitRow.text())
            else:
                nrows_preview = 100

            if self.lineEdit_limitRow.text() == "全部":
                nrows = 100000000
            else:
                nrows = int(self.lineEdit_limitRow.text())

            encoding = self.comboBox_encode.currentText()
            skiprows = int(self.lineEdit_passHead.text())

            logging.info("file_path：{}，header：{}，skiprows：{}，nrows：{}".format(
                self.file_path, header, skiprows, nrows_preview))

            self.current_dataset = pd.read_spss(self.file_path)

            if len(self.current_dataset) == 0:
                self.tableWidget_previewData.clear()
                logging.info("当前有效数据为空")
            else:
                self.import_dateset_preview()
                logging.info("数据导入成功")

    def import_dateset_preview(self):
        """
        刷新预览数据
        """
        if len(self.current_dataset) > 0:
            input_table_rows = self.current_dataset.head(100).shape[0]
            input_table_colunms = self.current_dataset.shape[1]
            input_table_header = self.current_dataset.columns.values.tolist()
            self.tableWidget_previewData.setColumnCount(input_table_colunms)
            self.tableWidget_previewData.setRowCount(input_table_rows)

            # 设置数据预览窗口的标题行
            table_header = []
            i = 1
            while i <= len(self.current_dataset.columns):
                table_header.append("C" + str(i))
                i += 1

            if self.checkBox_ifColumns.isChecked():
                self.tableWidget_previewData.setHorizontalHeaderLabels(input_table_header)
            else:
                self.tableWidget_previewData.setHorizontalHeaderLabels(table_header)
                self.current_dataset.columns = table_header
            # 数据预览窗口
            for i in range(input_table_rows):
                input_table_rows_values = self.current_dataset.iloc[[i]]
                input_table_rows_values_array = np.array(input_table_rows_values)
                input_table_rows_values_list = input_table_rows_values_array.tolist()[0]
                for j in range(input_table_colunms):
                    input_table_items_list = input_table_rows_values_list[j]

                    input_table_items = str(input_table_items_list)
                    newItem = QTableWidgetItem(input_table_items)
                    newItem.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                    self.tableWidget_previewData.setItem(i, j, newItem)


class ImportSasForm(ImportDialog, SASImport_Ui_Form):
    """
    打开"从sas导入"窗口
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.center()

        self.file_path = ''
        self.current_dataset_name = ""
        self.current_dataset = pd.DataFrame()

        # 导入窗口的相关事件
        # 在"导入"窗口，打开选择文件
        self.pushButton_choosefile.clicked.connect(self.openFile)
        # 展示数据
        self.checkBox_ifColumns.stateChanged.connect(self.import_dateset_reload)

        self.comboBox_encode.currentTextChanged.connect(self.import_dateset_reload)
        self.lineEdit_filePath.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_passHead.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_datasetName.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_limitRow.textChanged.connect(self.import_dateset_reload)
        self.lineEdit_missValue.textChanged.connect(self.import_dateset_reload)

        # 更新数据
        self.pushButton_ok.clicked.connect(self.import_send_dataset)
        self.pushButton_cancel.clicked.connect(self.close)

    def file_path_init(self, file_path):
        """
        #初始化sas文件路径
        """
        self.file_path = file_path
        if len(self.file_path) != 0:
            self.lineEdit_filePath.setText(self.file_path)

            self.current_dataset_name = os.path.split(self.lineEdit_filePath.text())[1]
            self.lineEdit_datasetName.setText(self.current_dataset_name)
            logging.info(
                "加载成功file_path{}，datasetName：{}".format(self.file_path, self.current_dataset_name))

            if len(self.file_path) > 0:
                if self.checkBox_ifColumns.isChecked():
                    header = 0
                else:
                    header = None
                # 仅预览前100条数据
                if self.lineEdit_limitRow.text() == "全部":
                    nrows_preview = 100
                elif int(self.lineEdit_limitRow.text()) <= 100:
                    nrows_preview = int(self.lineEdit_limitRow.text())
                else:
                    nrows_preview = 100

                if self.lineEdit_limitRow.text() == "全部":
                    nrows = 100000000
                else:
                    nrows = int(self.lineEdit_limitRow.text())

                encoding = self.comboBox_encode.currentText()
                skiprows = int(self.lineEdit_passHead.text())

                logging.info("file_path：{}，header：{}，skiprows：{}，nrows：{}".format(
                    self.file_path, header, skiprows, nrows_preview))

                self.current_dataset = pd.read_sas(self.file_path, format='sas7bdat', encoding=encoding)

                if len(self.current_dataset) == 0:
                    self.tableWidget_previewData.clear()
                    logging.info("当前有效数据为空")
                else:
                    self.import_dateset_preview()
                    logging.info("数据导入成功")
            else:
                print("请选择数据集")

    def openFile(self):
        """
        选择文件
        """
        openfile_name = QFileDialog.getOpenFileName(self, '选择文件', Settings.get_instance()['work_dir'],
                                                    'SAS文件(*.sas7bdat)')
        logging.info(openfile_name)

        self.file_path = openfile_name[0]
        self.lineEdit_filePath.setText(self.file_path)

        self.current_dataset_name = os.path.split(self.lineEdit_filePath.text())[1]
        self.lineEdit_datasetName.setText(self.current_dataset_name)
        logging.info("加载成功file_path{}，datasetName：{}".format(openfile_name, self.current_dataset_name))
        self.import_dateset_reload()

    def import_dateset_reload(self):
        """
        刷新导入的数据
        """
        header = 0
        nrows_preview = 100
        sep = ','
        skiprows = 0

        if len(self.file_path) > 0:
            if self.checkBox_ifColumns.isChecked():
                header = 0
            else:
                header = None
            # 仅预览前100条数据
            if self.lineEdit_limitRow.text() == "全部":
                nrows_preview = 100
            elif int(self.lineEdit_limitRow.text()) <= 100:
                nrows_preview = int(self.lineEdit_limitRow.text())
            else:
                nrows_preview = 100

            if self.lineEdit_limitRow.text() == "全部":
                nrows = 100000000
            else:
                nrows = int(self.lineEdit_limitRow.text())

            encoding = self.comboBox_encode.currentText()
            skiprows = int(self.lineEdit_passHead.text())

            logging.info("file_path：{}，header：{}，skiprows：{}，nrows：{}".format(
                self.file_path, header, skiprows, nrows_preview))

            self.current_dataset = pd.read_sas(self.file_path, format='sas7bdat', encoding=encoding)

            if len(self.current_dataset) == 0:
                self.tableWidget_previewData.clear()
                logging.info("当前有效数据为空")
            else:
                self.import_dateset_preview()
                logging.info("数据导入成功")

    def import_dateset_preview(self):
        """
        刷新预览数据
        """
        if len(self.current_dataset) > 0:
            input_table_rows = self.current_dataset.head(100).shape[0]
            input_table_colunms = self.current_dataset.shape[1]
            input_table_header = self.current_dataset.columns.values.tolist()
            self.tableWidget_previewData.setColumnCount(input_table_colunms)
            self.tableWidget_previewData.setRowCount(input_table_rows)

            # 设置数据预览窗口的标题行
            table_header = []
            i = 1
            while i <= len(self.current_dataset.columns):
                table_header.append("C" + str(i))
                i += 1

            if self.checkBox_ifColumns.isChecked():
                self.tableWidget_previewData.setHorizontalHeaderLabels(input_table_header)
            else:
                self.tableWidget_previewData.setHorizontalHeaderLabels(table_header)
                self.current_dataset.columns = table_header
            # 数据预览窗口
            for i in range(input_table_rows):
                input_table_rows_values = self.current_dataset.iloc[[i]]
                input_table_rows_values_array = np.array(input_table_rows_values)
                input_table_rows_values_list = input_table_rows_values_array.tolist()[0]
                for j in range(input_table_colunms):
                    input_table_items_list = input_table_rows_values_list[j]

                    input_table_items = str(input_table_items_list)
                    newItem = QTableWidgetItem(input_table_items)
                    newItem.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                    self.tableWidget_previewData.setItem(i, j, newItem)


class ImportMatlabForm(ImportDialog, MATLABImport_Ui_Form):
    """
    "从matlab导入"数据集到工作空间
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.center()

        self.file_path = ''  # 当前文件路径
        self.current_dataset_name = ""  # 当前数据名
        self.current_dataset = pd.DataFrame()  # 当前数据
        self.current_dataset_preview = pd.DataFrame()  # 当前数据副本，用于数据预览

        # 绑定事件
        self.pushButton_choosefile.clicked.connect(self.openFile)  # 在"导入"窗口，打开选择文件
        self.comboBox_var_name.currentIndexChanged.connect(self.import_dateset_reload)
        self.comboBox_encode.currentIndexChanged.connect(self.import_dateset_reload)
        self.lineEdit_filePath.textChanged.connect(self.import_dateset_reload)
        self.spinBox_limitRow.valueChanged.connect(self.import_dateset_reload)
        self.lineEdit_datasetName.textChanged.connect(self.import_dateset_reload)
        self.spinBox_passHead.valueChanged.connect(self.import_dateset_reload)
        self.lineEdit_missValue.textChanged.connect(self.import_dateset_reload)

        # 更新数据
        self.pushButton_ok.clicked.connect(self.import_send_dataset)
        self.pushButton_all.clicked.connect(self.import_send_all_dataset)
        self.pushButton_cancel.clicked.connect(self.close)

    def file_path_init(self, file_path):
        """
        #初始化matlab文件路径
        """
        self.file_path = file_path
        if len(self.file_path) != 0:
            self.lineEdit_filePath.setText(self.file_path)

            self.current_dataset_name = os.path.split(self.lineEdit_filePath.text())[1]
            self.lineEdit_datasetName.setText(self.current_dataset_name)
            logging.info(
                "加载成功file_path{}，datasetName：{}".format(self.file_path, self.current_dataset_name))

            if len(self.file_path) > 0:
                # 仅预览前100条数据
                if self.spinBox_limitRow.value() == 0:
                    nrows_preview = 100
                elif self.spinBox_limitRow.value() <= 100:
                    nrows_preview = self.spinBox_limitRow.value()
                else:
                    nrows_preview = 100

                if self.spinBox_limitRow.value() == "全部":
                    nrows = 100000000
                else:
                    nrows = self.spinBox_limitRow.value()

                encoding = self.comboBox_encode.currentText()
                skiprows = self.spinBox_passHead.value()
                logging.info("file_path：{}，skiprows：{}，nrows：{}".format(
                    self.file_path, skiprows, nrows_preview))

                try:
                    matlab_dict = read_matlab(self.file_path)

                    # 添加多个数据名到下拉列表
                    self.comboBox_var_name.clear()
                    for x in list(matlab_dict.keys()):
                        if x[:2] != '__':
                            self.comboBox_var_name.addItem(x)

                    # 默认仅预览第一个数据集
                    for x in list(matlab_dict.keys()):
                        if x[:2] != '__':
                            self.current_dataset = dataframe(matlab_dict[x]).head(nrows_preview)
                            self.current_dataset.columns = self.current_dataset.columns.astype('str')
                            break

                    if len(self.current_dataset) == 0:
                        self.tableWidget_previewData.clear()
                        logging.info("当前有效数据为空")
                    else:
                        self.import_dateset_preview()
                        logging.info("数据导入成功")
                except:
                    reply = QMessageBox.warning(self, '错误提示', '导入数据异常，请检查数据格式是否正确！', QMessageBox.Yes)
                    if reply == QMessageBox.Yes:
                        self.close()


        else:
            print("请选择数据集")

    def openFile(self):
        """
        选择文件
        """
        openfile_name = QFileDialog.getOpenFileName(self, '选择文件', Settings.get_instance()['work_dir'],
                                                    'MATLAB文件(*.mat)')
        logging.info(openfile_name)

        self.file_path = openfile_name[0]
        self.lineEdit_filePath.setText(self.file_path)

        self.current_dataset_name = os.path.split(self.lineEdit_filePath.text())[1]
        self.lineEdit_datasetName.setText(self.current_dataset_name)
        logging.info("加载成功file_path{}，datasetName：{}".format(openfile_name, self.current_dataset_name))

        matlab_dict = read_matlab(self.file_path)

        # 添加多个数据名到下拉列表
        self.comboBox_var_name.clear()
        for x in list(matlab_dict.keys()):
            if x[:2] != '__':
                self.comboBox_var_name.addItem(x)

    def import_dateset_reload(self):
        """
        刷新导入的数据
        """
        nrows_preview = 100
        skiprows = 0

        if len(self.file_path) > 0:
            # 仅预览前100条数据
            if self.spinBox_limitRow.value() == 0:
                nrows_preview = 100
            elif self.spinBox_limitRow.value() <= 100:
                nrows_preview = self.spinBox_limitRow.value()
            else:
                nrows_preview = 100

            if self.spinBox_limitRow.value() == "0":
                nrows = 100000000
            else:
                nrows = int(self.spinBox_limitRow.value())

            encoding = self.comboBox_encode.currentText()
            skiprows = int(self.spinBox_passHead.value())

            logging.info("file_path：{}，skiprows：{}，nrows：{}".format(self.file_path, skiprows, nrows_preview))

            try:
                matlab_dict = read_matlab(self.file_path)

                # 默认仅预览当前列表中的数据集
                current_var = self.comboBox_var_name.currentText()
                if len(current_var) > 0:
                    self.current_dataset = dataframe(matlab_dict[current_var])
                    self.current_dataset_preview = self.current_dataset[skiprows:].head(nrows_preview)
                    self.current_dataset.columns = self.current_dataset.columns.astype('str')

                if len(self.current_dataset) == 0:
                    self.tableWidget_previewData.clear()
                    logging.info("当前有效数据为空")
                else:
                    self.import_dateset_preview()
                    logging.info("数据导入成功")
            except:
                reply = QMessageBox.warning(self, '错误提示', '导入数据异常，请检查数据格式是否正确！', QMessageBox.Yes)
                if reply == QMessageBox.Yes:
                    self.close()

    def import_dateset_preview(self):
        """
        刷新预览数据
        """
        if len(self.current_dataset_preview) > 0:
            input_table_rows = self.current_dataset_preview.shape[0]
            input_table_colunms = self.current_dataset_preview.shape[1]
            input_table_header = self.current_dataset_preview.columns.astype('str')
            self.tableWidget_previewData.setColumnCount(input_table_colunms)
            self.tableWidget_previewData.setRowCount(input_table_rows)

            # 设置数据预览窗口的标题行
            self.tableWidget_previewData.setHorizontalHeaderLabels(input_table_header)

            # 数据预览窗口
            for i in range(input_table_rows):
                for j in range(input_table_colunms):
                    item = str(self.current_dataset_preview.iloc[i, j])
                    newItem = QTableWidgetItem(item)
                    newItem.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                    self.tableWidget_previewData.setItem(i, j, newItem)

    def import_send_all_dataset(self):
        """
        导入全部matlab数据到工作空间
        """
        from pyminer2.workspace.datamanager.datamanager import data_manager

        matlab_dict = read_matlab(self.file_path)
        for x in list(matlab_dict.keys()):
            if x[:2] != '__':
                self.current_dataset = dataframe(matlab_dict[x])
                if len(self.current_dataset) > 0:
                    if x in data_manager.varset.keys():
                        reply = QMessageBox.information(self, "提示", "变量名{0}已存在,是否覆盖？".format(x),
                                                        QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
                        if reply == QMessageBox.Yes:
                            var_name = x
                            continue
                        else:
                            name, ok = QInputDialog.getText(self, "变量名", "输入新的变量名称:", QLineEdit.Normal, var_name)
                            if ok and (len(name) != 0):
                                var_name = name
                    else:
                        var_name = x

                    data_manager.set_var(var_name, self.current_dataset)  # 将数据导入工作空间
                    self.close()
                else:
                    self.close()
