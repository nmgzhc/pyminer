from flask import Flask, request, jsonify, json, Response
from flask import render_template
import os
import binascii
from tools import tools
from flask_cors import CORS

app = Flask(__name__)
CORS(app, supports_credentials=True)  # 解决跨域


@app.route('/qt_vditor', methods=['POST'])
def render():
    if request.method == 'POST':
        data = request.get_data()
        json_data = json.loads(data)
        md_content = json_data['md_content']
        result = tools.convert_local_md_image(md_content, url="http://127.0.0.1:5000/qt_vditor/pictures/",
                                              md_path=os.path.dirname(json_data['md_path']))
        print(result)
        return render_template("qt_vditor/index.html", md_content=result)


@app.route('/qt_vditor/pictures/<path>', methods=['GET'])
def render_picture(path):
    if request.method == 'GET':
        hex = path.encode('utf-8')
        str_bin = binascii.unhexlify(hex)
        result = str_bin.decode('utf-8')
        if "!!!" in result:
            result = os.path.join(result.split('!!!')[0], result.split('!!!')[1]).replace('\\','/')
        if os.path.exists(result):
            with open(result, 'rb') as f:
                image = f.read()
            mdict = {
                    'jpeg': 'image/jpeg',
                    'jpg': 'image/jpeg',
                    'png': 'image/png',
                    'gif': 'image/gif'
                }
            mime = mdict[result.split('.')[-1]]
            return Response(image, mimetype=mime)
        return result, 200


@app.route('/qt_vditor/save', methods=['POST'])
def save_file():
    if request.method == 'POST':
        data = request.get_data()
        json_data = json.loads(data)
        content = json_data['md_content']
        file_path = json_data['filepath']
        if os.path.exists(file_path):
            with open(file_path, 'w', encoding='utf-8') as f:
                f.write(content)
        return 'ok'


@app.route('/qt_vditor/upload_file', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        # print(request.get_data())
        # print(request.data)
        # print(request.form)
        # print(request.form.to_dict())
        # print(request.json)
        # print(request.files)
        # print(request.files.get('file[]'))
        # print(request.files.get('file[]').filename)
        # data = request.form
        # print(data)
        # print(data.get('key'))
        # print(request.files)
        # print(request.json)
        # print(data.to_dict())
        file = request.files.get('file[]')
        path = os.path.join(os.path.dirname(__file__), 'static')
        filename = request.files.get('file[]').filename
        filepath = os.path.join(path, filename)
        data = {
            "msg": "ok",
            "code": 0,
            "data": {
                "succMap": {
                    filename: filepath,
                }
            }
        }
        file.save(filepath)
        return data, 200


if __name__ == '__main__':
    app.run(debug=True)
