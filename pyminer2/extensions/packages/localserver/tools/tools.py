import re
import binascii
import os
from flask import jsonify, Response


def convert_hexstr(m, url: str, md_path: str):
    md_path_hex = binascii.hexlify((md_path + "!!!").encode('utf-8')).decode('utf-8')
    if m.group(2).startswith('http'):
        result = m.group(2)
    elif m.group(2)[1]==':' or m.group(2)[0] == '/':
        result = url + binascii.hexlify(m.group(2).encode('utf-8')).decode('utf-8')
    else:
        result = url + md_path_hex + binascii.hexlify(m.group(2).encode('utf-8')).decode('utf-8')
    return "![{}](".format(m.group(1)) + result + ")"


def from_hexstr(m, url: str):
    result = m.group(2)
    if m.group(2).startswith(url):
        hex_str = m.group(2).lstrip(url)
        hex = hex_str.encode('utf-8')
        str_bin = binascii.unhexlify(hex)
        result = str_bin.decode('utf-8')
        if "!!!" in result:
            result = result.split("!!!")[-1]
    return "![{}](".format(m.group(1)) + result + ")"


def convert_local_md_image(md_content, url, md_path):
    pattern = re.compile(r'(?:!\[(.*?)\]\((.*?)\))')
    new_content = pattern.sub(lambda x: convert_hexstr(x, url, md_path), md_content)
    return new_content


def from_local_md_image(md_content, url):
    pattern = re.compile(r'(?:!\[(.*?)\]\((.*?)\))')
    new_content = pattern.sub(lambda x: from_hexstr(x, url), md_content)
    return new_content


def get_image(md_path, uri):
    imgPath = os.path.join(md_path, uri)
    mdict = {
        'jpeg': 'image/jpeg',
        'jpg': 'image/jpeg',
        'png': 'image/png',
        'gif': 'image/gif'
    }
    mime = mdict[uri.split('.')[-1]]
    if os.path.exists(uri):
        with open(uri, 'rb') as f:
            image = f.read()
        return Response(image, mimetype=mime)
    if os.path.exists(imgPath):
        with open(imgPath, 'rb') as f:
            image = f.read()
        return Response(image, mimetype=mime)
    return "", 404


if __name__ == '__main__':
    md_content = """
    # abc
    aa ![xxx](http://xxx.jpg)
    aa ![xxx](aa/中文)
    aa ![xxx](d:/cc)dd
    """
    result = convert_local_md_image(md_content, url="http://127.0.0.1/qt_vditor/pictures/", md_path='D:/a.md')
    print(result)
    print(from_local_md_image(result, url="http://127.0.0.1/qt_vditor/pictures/"))
