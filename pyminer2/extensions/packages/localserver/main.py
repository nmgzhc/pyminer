# -*- coding: utf-8 -*-
# @Time    : 2020/9/1 20:47
# @Author  : 别着急慢慢来
# @FileName: main.py
# from pyminer2.extensions.packages.ipython_console.main import ConsoleInterface
# from pyminer2.extensions.extensionlib import extension_lib
import os
import sys
import logging

logger = logging.getLogger('localserver')
from pyminer2.extensions.extensionlib import BaseExtension, BaseInterface
from .server import app
from pmgwidgets import PMGOneShotThreadRunner
import threading


class Extension(BaseExtension):
    def on_load(self):
        self.thread = threading.Thread(target=app.run)  # 这个线程与主界面没有任何交互，直接用系统内建的线程库即可，保证其安全性。
        self.thread.setDaemon(True)
        self.thread.start()
        logger.info('flask已启动')


class Interface(BaseInterface):
    def hello(self):
        print("Hello")
