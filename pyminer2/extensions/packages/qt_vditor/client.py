import os
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWebEngineWidgets import QWebEngineView
import sys
import requests
import json
from PyQt5.QtWebChannel import QWebChannel


class Window(QtWidgets.QDialog):
    def __init__(self, url: str, title: str = '', file_path='', raw_content: str = ''):
        super().__init__()
        self.file_path = file_path
        if file_path and os.path.exists(file_path):
            (filename, ext) = os.path.basename(file_path).split('.')
            if ext == 'md':
                self.title = filename
                with open(file_path, 'r', encoding='utf-8') as f:
                    self.raw_content = f.read()
        else:
            self.title = title
            self.raw_content = raw_content
        self.setWindowTitle('markdown 编辑器')
        layout = QtWidgets.QVBoxLayout()
        self.pushButton = QtWidgets.QPushButton('save')
        self.pushButton.clicked.connect(self.save_file)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
        view = QWebEngineView()
        response = self.post(url)
        view.setHtml(response.text)
        layout.addWidget(self.pushButton)
        layout.addWidget(view)
        view.show()
        self.web_engine_view = view

    def post(self, url):
        data = {
            'md_content': self.raw_content,
            'md_path':self.file_path
        }
        headers = {'Content-Type': 'application/json'}
        response = requests.post(url=url, headers=headers, data=json.dumps(data))
        return response

    def load_file(self, file_path: str):
        self.file_path=file_path
        if file_path and os.path.exists(file_path):
            (filename, ext) = os.path.basename(file_path).split('.')
            if ext == 'md':
                self.title = filename
                with open(file_path, 'r', encoding='utf-8') as f:
                    self.raw_content = f.read()
                response = self.post('http://127.0.0.1:5000/qt_vditor')
                self.web_engine_view.setHtml(response.text)

    def save_file(self):
        self.web_engine_view.page().runJavaScript("saveMDText(\"{}\")".format(self.file_path))
        # if os.path.exists(self.file_path):
        #     with open(self.file_path, 'w', encoding='utf-8') as f:
        #         print(self.new_content)
        #         f.write(self.new_content)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    win = Window(url='http://127.0.0.1:5000/qt_vditor',
                 file_path='')
    win.load_file(file_path='D:/projects/pyminer/pyminer2/extensions/packages/pmagg/sample/sample_code.md')
    win.show()
    app.exec()
