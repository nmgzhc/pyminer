# -*- coding: utf-8 -*-
# @Time    : 2020/9/1 20:47
# @Author  : 别着急慢慢来
# @FileName: main.py
# from pyminer2.extensions.packages.ipython_console.main import ConsoleInterface
# from pyminer2.extensions.extensionlib import extension_lib
import logging
logger = logging.getLogger('qt_vditor')
from pyminer2.extensions.extensionlib import BaseExtension, BaseInterface
from ..localserver import server


class Extension(BaseExtension):
    def on_load(self):
        logger.info('默认使用 qt vditor')


class Interface(BaseInterface):
    def hello(self):
        print("Hello")
