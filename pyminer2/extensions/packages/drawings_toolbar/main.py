import os, sys
import logging
from typing import TYPE_CHECKING, Callable, Any
from numpy import ndarray
from pandas import Series, DataFrame
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.Qt import *
import numpy as np
import pandas as pd
from pyminer2.extensions.extensionlib import pmwidgets
from pmgwidgets import PMGToolBar, create_icon
from pyminer2.extensions.extensionlib import BaseExtension, BaseInterface
from pyminer2.extensions.packages.drawings_toolbar.group_chart import DialogGroup
from pyminer2.extensions.packages.pmagg import PMAgg
from matplotlib import pyplot as plt

logger = logging.getLogger(__name__)
_ = lambda s: s
if TYPE_CHECKING:
    from pyminer2.extensions.extensionlib import extension_lib


class PMMenuToolPanel(QFrame):
    """
    面板控件，用于放置绘图按钮或其他插件按钮
    """

    def __init__(self):
        super(PMMenuToolPanel, self).__init__()
        self.setup_ui()

    def setup_ui(self):
        self.setMinimumSize(QtCore.QSize(500, 85))
        self.setMaximumSize(QtCore.QSize(16777215, 85))
        self.setObjectName("frame")
        self.hbox = QHBoxLayout()
        self.hbox.setContentsMargins(0, 0, 0, 0)
        self.hbox.setSpacing(0)

        self.widget_panel = QWidget()
        self.widget_panel.setStyleSheet("margin:1px;")
        self.widget_panel_hbox = QHBoxLayout()
        self.widget_panel_hbox.setContentsMargins(0, 0, 0, 0)
        self.widget_panel_hbox.setSpacing(10)
        self.widget_panel.setLayout(self.widget_panel_hbox)

        self.hspace = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.btn_down = QToolButton()
        self.btn_down.setObjectName("btn_tool_select")
        self.btn_down.setToolTip("查看更多")
        self.btn_down.setMinimumSize(QtCore.QSize(25, 85))
        self.btn_down.setMaximumSize(QtCore.QSize(25, 85))
        self.btn_down.setStyleSheet(
            "#btn_tool_select{border:1px solid rgb(189,189,189);border-top-left-radius:0px;border-top-right-radius:5px;border-bottom-left-radius:0px;border-bottom-right-radius:5px;background-color: rgb(230,230,230);padding:0px 0px 0px 0px;}#btn_tool_select:hover{background:lightgray;}")

        self.current_path = os.path.dirname(__file__)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(os.path.join(self.current_path, 'source/down.svg')), QtGui.QIcon.Normal,
                        QtGui.QIcon.Off)
        self.btn_down.setIcon(icon1)

        self.btn_down.setAutoRaise(True)

        # 添加按钮和弹簧到水平布局
        self.hbox.addWidget(self.widget_panel)
        self.hbox.addItem(self.hspace)
        self.hbox.addWidget(self.btn_down)
        self.setLayout(self.hbox)
        self.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.setFrameShadow(QtWidgets.QFrame.Raised)
        self.setLineWidth(1)
        self.dialog_grp = DialogGroup()
        self.setStyleSheet(
            "#frame{border:1px solid rgb(189,189,189);padding: -5px -10px 0px 20px;margin: 0px 0px 0px 0px;border-radius:5px;}")

        self.btn_down.clicked.connect(self.close)
        self.draw_funcs = {'line_chart': lambda: print('draw line chart!'),
                           'bar_chart': lambda: print('draw bar chart!'),
                           'pie_chart': lambda: print('draw pie chart!'),
                           'barh_chart': lambda: print('draw barh chart!'),
                           'stack_chart': lambda: print('draw stack chart!'),
                           'scatter_chart': lambda: print('draw scatter chart!'),
                           'box_chart': lambda: print('draw box chart!'),
                           'hist_chart': lambda: print('draw hist chart!'),
                           'radar_chart': lambda: print('draw radar chart!'),
                           'heap_chart': lambda: print('draw heap chart!'),
                           'step_chart': lambda: print('draw step chart!'),
                           'group_chart': lambda: print('draw group chart!')}
        # 测试添加按钮
        self.add_button("柱形图", os.path.join(self.current_path, 'source/柱形图.png'), lambda: self.draw('bar_chart'))
        self.add_button("折线图", os.path.join(self.current_path, 'source/折线图.png'), lambda: self.draw('line_chart'))
        self.add_button("饼图", os.path.join(self.current_path, 'source/饼图.png'), lambda: self.draw('pie_chart'))
        self.add_button("条形图", os.path.join(self.current_path, 'source/条形图.png'), lambda: self.draw('barh_chart'))
        self.add_button("面积图", os.path.join(self.current_path, 'source/面积图.png'), lambda: self.draw('stack_chart'))
        self.add_button("气泡图", os.path.join(self.current_path, 'source/气泡图.png'), lambda: self.draw('scatter_chart'))
        self.add_button("箱线图", os.path.join(self.current_path, 'source/箱线图.png'), lambda: self.draw('box_chart'))
        self.add_button("直方图", os.path.join(self.current_path, 'source/直方图.png'), lambda: self.draw('hist_chart'))
        self.add_button("雷达图", os.path.join(self.current_path, 'source/雷达图.png'), lambda: self.draw('radar_chart'))
        self.add_button("热力图", os.path.join(self.current_path, 'source/热力图.png'), lambda: self.draw('heap_chart'))
        self.add_button("地图", os.path.join(self.current_path, 'source/地图.png'), lambda: self.draw('step_chart'))
        self.add_button("组合图", os.path.join(self.current_path, 'source/组合图.png'), lambda: self.draw('group_chart'))

    def add_button(self, btn_text: str, icon_path: str, btn_action: Callable) -> None:
        sub_widget = QToolButton()
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(icon_path), QtGui.QIcon.Normal,
                       QtGui.QIcon.Off)
        sub_widget.setIcon(icon)
        sub_widget.setIconSize(QtCore.QSize(50, 40))
        sub_widget.setMinimumSize(QtCore.QSize(85, 75))
        sub_widget.setMaximumSize(QtCore.QSize(85, 75))
        sub_widget.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        sub_widget.setAutoRaise(True)
        sub_widget.setText(btn_text)
        self.widget_panel_hbox.addWidget(sub_widget)
        sub_widget.clicked.connect(btn_action)

    def draw(self, chart_type: str):
        draw_chart_func = self.draw_funcs.get(chart_type)
        if draw_chart_func is not None:
            draw_chart_func()


class ToolbarBlock(QWidget):
    def __init__(self, parent=None):
        global _
        super(ToolbarBlock, self).__init__(parent=parent)
        layout = QVBoxLayout()
        h_layout = QHBoxLayout()
        layout.addLayout(h_layout)
        self.annotation_label = QLabel(_('Variable Selected'))
        variable_show_label = QLabel(_('No Variable'))
        h_layout.addWidget(variable_show_label)
        variable_show_label.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Expanding)
        variable_show_label.setMaximumWidth(100)

        variable_show_label.setAlignment(Qt.AlignCenter)
        self.variable_show_label = variable_show_label

        layout.addWidget(self.annotation_label)
        layout.setContentsMargins(0, 0, 0, 0)
        h_layout.setContentsMargins(0, 0, 0, 0)
        self.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
        self.annotation_label.setMaximumHeight(20)
        self.annotation_label.setMinimumHeight(20)
        self.annotation_label.setAlignment(Qt.AlignBottom | Qt.AlignHCenter)


class ButtonMappedToItem(QToolButton):
    """
    这个按钮类的意思是，将按钮与QListWidget的条目一一对应起来
    当使用循环生成按钮的时候，按钮没有办法通过匿名函数或者函数传参的方式进行类的传递，所以只能给每个按钮的对象都分别绑定一个QListWidgetitem

    """

    def __init__(self, parent: 'PMDrawingsToolBar', item: 'QListWidgetItem'):
        super().__init__(parent)
        self.item: QListWidgetItem = item
        self.parent: 'PMDrawingsToolBar' = parent
        self.clicked.connect(self.on_mouse_clicked)

    def on_mouse_clicked(self):
        self.parent.on_item_double_clicked(self.item)


class PMDrawingsToolBar(PMGToolBar):
    drawing_item_double_clicked_signal: 'pyqtSignal' = pyqtSignal(str)
    extension_lib: 'extension_lib' = None
    variable = None

    def __init__(self):
        super().__init__()
        self.selected_var_show_label = QLabel()
        self.selected_var_show_label.setText('var:')

        tb_block = ToolbarBlock()
        self.toolbar_block = tb_block
        self.add_widget('variable_show_block', tb_block)
        self._control_widget_dic['variable_show_label'] = tb_block.variable_show_label

        # self._control_widget_dic[
        #     'drawing_selection_panel'] = pmwidgets.TopLevelWidget(self)

        # drawing_selection_panel: 'pmgwidgets.TopLevelWidget' \
        #     = self._control_widget_dic['drawing_selection_panel']
        # drawing_selection_panel.set_central_widget(QListWidget())

        self.addSeparator()
        self.drawing_button_bar = PMMenuToolPanel()

        self.dialog_group = DialogGroup()
        self.dialog_group.btn_combin.clicked.connect(self.draw_group_chart)
        self.dialog_group.cb_select1.currentIndexChanged.connect(self.cb1_change)
        self.dialog_group.cb_select2.currentIndexChanged.connect(self.cb2_change)

        self.draw_fig = PMAgg.Window()

        self._control_widget_dic['button_list'] = self.drawing_button_bar
        self.addWidget(self.drawing_button_bar)
        self.drawing_button_bar.setSizePolicy(
            QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.drawing_button_bar.draw_funcs['line_chart'] = self.draw_line_chart
        self.drawing_button_bar.draw_funcs['bar_chart'] = self.draw_bar_chart
        self.drawing_button_bar.draw_funcs['pie_chart'] = self.draw_pie_chart
        self.drawing_button_bar.draw_funcs['barh_chart'] = self.draw_barh_chart
        self.drawing_button_bar.draw_funcs['stack_chart'] = self.draw_stack_chart
        self.drawing_button_bar.draw_funcs['scatter_chart'] = self.draw_scatter_chart
        self.drawing_button_bar.draw_funcs['box_chart'] = self.draw_box_chart
        self.drawing_button_bar.draw_funcs['hist_chart'] = self.draw_hist_chart
        self.drawing_button_bar.draw_funcs['radar_chart'] = self.draw_radar_chart
        self.drawing_button_bar.draw_funcs['heap_chart'] = self.draw_heap_chart
        self.drawing_button_bar.draw_funcs['step_chart'] = self.draw_step_chart
        self.drawing_button_bar.draw_funcs['group_chart'] = self.draw_group_before

    def on_data_selected(self, data_name: str):
        """
        当变量树中的数据被单击选中时，调用这个方法。
        """
        self.toolbar_block.variable_show_label.setText('%s' % data_name)
        self.variable = self.extension_lib.Data.get_var(data_name)
        logger.info('Variable clicked. Name is \'' + data_name + '\' , value is ' + repr(self.variable))

    def del_more_strip(self, string: str):
        """删除空格并变成逗号"""
        str_list = string.split(' ')
        new_str_list = []
        for ns in str_list:
            if ns != '':
                new_str_list.append(ns)
        return ','.join(new_str_list)

    def prepare_draw_data(self, draw_data):
        """对画图的数据进行检测，如果是2维的int,float数据类型可以进行绘图,
        现在只支持对list,dict,tuple,ndarray,Series,DataFrame数据类型进行绘图"""
        if isinstance(draw_data, (list, tuple)):
            draw_value = np.array(draw_data)
        elif isinstance(draw_data, dict):
            dv = pd.Series(draw_data)
            draw_value = np.array(dv.values)
        elif isinstance(draw_data, (Series, DataFrame)):
            draw_value = np.array(draw_data.values)
        elif isinstance(draw_data, ndarray):
            draw_value = draw_data
        else:  # 如果不是这些类型提示数据类型错误
            self.extension_lib.get_interface('ipython_console').run_command(
                command='', hint_text='ERROR: Data type is wrong!', hidden=False)
            return False

        int_float_types = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
        if draw_value.dtype in int_float_types:
            ndim = draw_value.ndim
            if ndim < 3:
                if 0 not in draw_value.shape:
                    return draw_value
            else:
                self.extension_lib.get_interface('ipython_console').run_command(
                    command='',
                    hint_text='ERROR: Please select 1D or 2D data', hidden=False)
                return False
        else:
            self.extension_lib.get_interface('ipython_console').run_command(
                command='',
                hint_text='ERROR: Data type is wrong', hidden=False)
            return False

    def cb1_change(self):
        """绘制提示窗口图形"""
        line_var_name = self.dialog_group.cb_select1.currentText()
        line_var = self.extension_lib.Data.get_var(line_var_name)
        self.line_real_value = self.prepare_draw_data(line_var)
        if self.line_real_value is not False:
            self.dialog_group.draw_line_widget.get_figure().gca().cla()
            ax_line = self.dialog_group.draw_line_widget.axes
            ax_line.plot(self.line_real_value, color='orange')
            self.dialog_group.draw_line_widget.draw()
        else:
            self.dialog_group.close()

    def cb2_change(self):
        bar_var_name = self.dialog_group.cb_select2.currentText()
        bar_var = self.extension_lib.Data.get_var(bar_var_name)
        self.bar_real_value = self.prepare_draw_data(bar_var)
        if self.bar_real_value is not False:
            self.dialog_group.draw_bar_widget.get_figure().gca().cla()
            ax_bar = self.dialog_group.draw_bar_widget.axes
            x_axis = [i for i in range(self.bar_real_value.shape[0])]
            ax_bar.bar(x_axis, self.bar_real_value)
            self.dialog_group.draw_bar_widget.draw()
        else:
            self.dialog_group.close()

    def draw_group_before(self):
        """绘制组合图1-显示提示窗口"""
        vars_name_list = self.extension_lib.Data.get_all_variable_names()
        self.dialog_group.cb_select1.clear()
        self.dialog_group.cb_select2.clear()
        if len(vars_name_list) > 9:
            new_vars = vars_name_list[8:]
            self.dialog_group.cb_select1.addItems(new_vars)
            self.dialog_group.cb_select1.setCurrentIndex(0)
            self.dialog_group.cb_select2.addItems(new_vars)
            self.dialog_group.cb_select2.setCurrentIndex(1)
            self.dialog_group.show()
        else:
            self.extension_lib.get_interface('ipython_console').run_command(
                command='',
                hint_text="ERROR：Can't draw chart with one variable!", hidden=False)

    def draw_group_chart(self):
        """绘制组合图2"""
        line_var_name = self.dialog_group.cb_select1.currentText()
        bar_var_name = self.dialog_group.cb_select2.currentText()
        if self.bar_real_value.ndim == 2:
            draw_value1 = self.bar_real_value.flatten()
        else:
            draw_value1 = self.bar_real_value
        if self.line_real_value.ndim == 2:
            draw_value2 = self.line_real_value.flatten()
        else:
            draw_value2 = self.line_real_value
        ncols1 = draw_value1.shape[0]
        ncols2 = draw_value2.shape[0]
        if ncols1 > ncols2:
            x_axis = [x for x in range(ncols2)]
            draw_value1 = draw_value1[:ncols2]
        else:
            x_axis = [x for x in range(ncols1)]
            draw_value2 = draw_value2[:ncols1]
        self.extension_lib.get_interface('ipython_console').run_command(
            command="plt.bar({},{},zorder=1);plt.plot({},{},zorder=2,color='orange');plt.show()".format(
                str(x_axis), self.del_more_strip(str(draw_value1)), str(x_axis),
                self.del_more_strip(str(draw_value2))),
            hint_text='plotting {} and {}'.format(line_var_name, bar_var_name), hidden=False)
        self.dialog_group.close()

    def draw_step_chart(self):
        """绘制阶梯图"""
        draw_value = self.prepare_draw_data(self.variable)
        if draw_value is not False:
            if draw_value.ndim == 2:
                draw_value = draw_value.flatten()
            ncols = draw_value.shape[0]
            x_axis = [x for x in range(ncols)]
            var_name = self.toolbar_block.variable_show_label.text()
            self.extension_lib.get_interface('ipython_console').run_command(
                command='plt.step({},{},where="pre");plt.show()'.format(
                    str(x_axis), self.del_more_strip(str(draw_value))),
                hint_text='plotting %s' % var_name, hidden=False)

    def draw_heap_chart(self):
        """绘制热力图"""
        draw_value = self.prepare_draw_data(self.variable)
        if draw_value is not False:
            if draw_value.ndim == 2:
                var_name = self.toolbar_block.variable_show_label.text()
                self.extension_lib.get_interface('ipython_console').run_command(
                    command='plt.imshow({});plt.show()'.format(self.del_more_strip(str(draw_value))),
                    hint_text='plotting %s' % var_name, hidden=False)
            else:
                self.extension_lib.get_interface('ipython_console').run_command(
                    command='',
                    hint_text='ERROR: Heap chart need 2D data', hidden=False)

    def draw_radar_chart(self):
        """绘制雷达图"""
        draw_value = self.prepare_draw_data(self.variable)
        if draw_value is not False:
            if draw_value.ndim == 2:
                draw_value = draw_value.flatten()
            ncols = draw_value.shape[0]
            angles = np.linspace(0, 2 * np.pi, ncols, endpoint=False)
            draw_value = np.concatenate((draw_value, [draw_value[0]]))
            angles = np.concatenate((angles, [angles[0]]))
            var_name = self.toolbar_block.variable_show_label.text()
            self.extension_lib.get_interface('ipython_console').run_command(
                command='plt.subplot(111,polar=True);plt.polar({},{},marker="o");'
                        'plt.show()'.format(self.del_more_strip(str(angles)),
                                            self.del_more_strip(str(draw_value))),
                hint_text='plotting %s' % var_name, hidden=False)

    def draw_hist_chart(self):
        """绘制直方图"""
        draw_value = self.prepare_draw_data(self.variable)
        if draw_value is not False:
            if draw_value.ndim == 2:
                draw_value = draw_value.flatten()
            var_name = self.toolbar_block.variable_show_label.text()
            self.extension_lib.get_interface('ipython_console').run_command(
                command='plt.hist({});plt.show()'.format(self.del_more_strip(str(draw_value))),
                hint_text='plotting %s' % var_name, hidden=False)

    def draw_box_chart(self):
        """ 绘制箱线图"""
        draw_value = self.prepare_draw_data(self.variable)
        if draw_value is not False:
            var_name = self.toolbar_block.variable_show_label.text()
            self.extension_lib.get_interface('ipython_console').run_command(
                command='plt.boxplot({},widths=0.25);plt.show()'.format(self.del_more_strip(str(draw_value))),
                hint_text='plotting %s' % var_name, hidden=False)

    def draw_scatter_chart(self):
        """绘制散点图"""
        draw_value = self.prepare_draw_data(self.variable)
        if draw_value is not False:
            var_name = self.toolbar_block.variable_show_label.text()
            if draw_value.ndim == 1:
                draw_value = draw_value.flatten()
                ncols = draw_value.shape[0]
                x_axis = [x for x in range(ncols)]
                self.extension_lib.get_interface('ipython_console').run_command(
                    command='plt.scatter({},{});plt.show()'.format(str(x_axis), self.del_more_strip(str(draw_value))),
                    hint_text='plotting %s' % var_name, hidden=False)
            elif draw_value.ndim == 2:
                if draw_value.shape[0] == 2:
                    draw_x = self.del_more_strip(str(draw_value[0]))
                    draw_y = self.del_more_strip(str(draw_value[1]))
                    self.extension_lib.get_interface('ipython_console').run_command(
                        command='plt.scatter({},{});plt.show()'.format(draw_x, draw_y),
                        hint_text='plotting %s' % var_name, hidden=False)
                elif draw_value.shape[0] == 3:
                    draw_x = self.del_more_strip(str(draw_value[0]))
                    draw_y = self.del_more_strip(str(draw_value[1]))
                    draw_s = self.del_more_strip(str(draw_value[2]))
                    self.extension_lib.get_interface('ipython_console').run_command(
                        command='plt.scatter({},{},s={});plt.show()'.format(draw_x, draw_y, draw_s),
                        hint_text='plotting %s' % var_name, hidden=False)

    def draw_stack_chart(self):
        """绘制面积图"""
        draw_value = self.prepare_draw_data(self.variable)
        if draw_value is not False:
            if draw_value.ndim == 2:
                draw_value = draw_value.flatten()
            ncols = draw_value.shape[0]
            x_axis = [x for x in range(ncols)]
            var_name = self.toolbar_block.variable_show_label.text()
            self.extension_lib.get_interface('ipython_console').run_command(
                command='plt.stackplot({},{});plt.show()'.format(str(x_axis),
                                                                 self.del_more_strip(str(draw_value))),
                hint_text='plotting %s' % var_name, hidden=False)

    def draw_pie_chart(self):
        """绘制饼图"""
        draw_value = self.prepare_draw_data(self.variable)
        if draw_value is not False:
            if draw_value.ndim == 2:
                draw_value = draw_value.flatten()
            if draw_value.shape[0] > 9:  # 大于9暂时不画
                self.console_message('ERROR: The length of data should not exceed 9', flag=True)
            else:
                var_name = self.toolbar_block.variable_show_label.text()
                self.extension_lib.get_interface('ipython_console').run_command(
                    command='plt.pie({},autopct="%3.1f%%");plt.show()'.format(self.del_more_strip(str(draw_value))),
                    hint_text='plotting %s' % var_name, hidden=False)

    def draw_bar_chart(self):
        """绘制柱状图"""
        bar_width = 0.6
        draw_value = self.prepare_draw_data(self.variable)
        if draw_value is not False:
            var_name = self.toolbar_block.variable_show_label.text()
            if (draw_value.ndim == 2 and draw_value.shape[0]) == 1 or draw_value.ndim == 1:
                draw_value = draw_value.flatten()
                ncols = draw_value.shape[0]
                x_axis = [x for x in range(ncols)]
                plt.bar(x_axis, draw_value, width=bar_width)
                plt.xticks(x_axis)
            if draw_value.ndim == 2 and draw_value.shape[0] < 6:
                bar_start = bar_width / 2
                ncols = draw_value.shape[1]
                bar_step = bar_width / draw_value.shape[0]
                x_lbl = [x for x in range(ncols)]
                x_axis = np.array([x for x in range(ncols)]) - bar_start
                for bar_n in range(draw_value.shape[0]):
                    plt.bar(x_axis, draw_value[bar_n], width=bar_step, align='edge')
                    x_axis = x_axis + bar_step
                plt.xticks(x_lbl)
            fig = plt.gcf()
            self.draw_fig.get_canvas(fig)
            self.draw_fig.show()
            self.console_message(var_name)

    def draw_barh_chart(self):
        """绘制条形图"""
        draw_value = self.prepare_draw_data(self.variable)
        if draw_value is not False:
            if draw_value.ndim == 2:
                draw_value = draw_value.flatten()
            ncols = draw_value.shape[0]
            x_axis = [x for x in range(ncols)]
            var_name = self.toolbar_block.variable_show_label.text()
            plt.barh(x_axis, draw_value)
            fig = plt.gcf()
            self.draw_fig.get_canvas(fig)
            self.draw_fig.show()
            self.console_message(var_name)

    def draw_line_chart(self):
        """
        绘制折线图
        """
        draw_value = self.prepare_draw_data(self.variable)
        if draw_value is not False:
            var_name = self.toolbar_block.variable_show_label.text()
            # if plt.gcf():
            #     plt.gca().cla()
            plt.plot(draw_value)
            fig = plt.gcf()
            self.draw_fig.get_canvas(fig)
            self.draw_fig.show()
            self.console_message(var_name)

    def console_message(self, var_name: str, flag=False):
        mess=''
        if flag:
            mess = var_name
        else:
            mess = f'plotting {var_name}'
        self.extension_lib.get_interface('ipython_console').run_command(command='',
                                                                        hint_text=mess, hidden=False)

    def on_data_modified(self, var_name: str, variable: Any, data_source: str):
        """
        在数据被修改时，调用这个方法。
        """
        pass

    def on_close(self):
        self.hide()
        self.deleteLater()

    def set_panel_visibility(self):
        self.refresh_pos()
        w: 'pmgwidgets.TopLevelWidget' = self._control_widget_dic['drawing_selection_panel']
        w.setVisible(not w.isVisible())

    def refresh_pos(self):
        """
        刷新顶上的ToplevelWidget的位置。
        """
        return
        btn = self.get_control_widget('button_show_more_plots')
        panel: 'pmgwidgets.TopLevelWidget' = self.get_control_widget(
            'drawing_selection_panel')
        width = self.get_control_widget('button_list').width()
        panel.set_width(width)
        panel.set_position(QPoint(btn.x() - width, btn.y()))

    # def bind_events(self):
    #     """
    #     绑定事件。这个将在界面加载完成之后被调用。
    #     """
    #
    #     drawing_list_widget: 'pmgwidgets.TopLevelWidget' = self.get_control_widget(
    #         'drawing_selection_panel')
    #     cw: QListWidget = drawing_list_widget.central_widget
    #     cw.itemDoubleClicked.connect(self.on_item_double_clicked)
    #
    #     self.extension_lib.Signal.get_window_geometry_changed_signal().connect(self.refresh_pos)
    #     self.extension_lib.Signal.get_close_signal().connect(self.on_close)

    def refresh_outer_buttons(self):
        """
        刷新显示在按纽条上面的按钮们。
        首先全部移除，然后添加进来。
        这些按钮不是由用户控制添加的，而是自动的呈现listwidget的前最多10项。
        """
        for w in self.drawing_button_bar.buttons:
            self.drawing_button_bar.button_layout.removeWidget(w)
        selection_list: 'QListWidget' = self.get_control_widget(
            'drawing_selection_panel').central_widget
        for i in range(10):
            item = selection_list.item(i)

            if item is None:
                break
            b = ButtonMappedToItem(self, item)
            b.setIcon(item.icon())
            b.setText(item.text())
            b.setToolTip(item.text())
            b.setMaximumWidth(60)
            b.setMaximumHeight(40)
            b.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

            self.drawing_button_bar.button_layout.addWidget(b)

    def add_toolbox_widget(self, name: str, text: str,
                           icon_path: str, hint: str = '', refresh=True):
        """
        将控件添加到工具箱，成为QListWidgetItem，通过这些Item生成对应的按钮。
        """
        item = QListWidgetItem('%s\n%s' % (text, hint))
        item.name = name
        cw: QListWidget = self.drawing_selection_panel.central_widget
        cw.addItem(item)
        item.setIcon(create_icon((icon_path)))
        cw.setIconSize(QSize(40, 40))
        item.setSizeHint(QSize(self.drawing_selection_panel.width - 20, 40))
        if refresh:
            self.refresh_outer_buttons()

    def on_item_double_clicked(self, widget_item: QListWidgetItem):

        listwidget: QListWidget = self.drawing_selection_panel.central_widget
        self.drawing_item_double_clicked_signal.emit(widget_item.name)


class Extension(BaseExtension):
    if TYPE_CHECKING:
        interface: 'DrawingsInterface' = None
        widget: 'PMDrawingsToolBar' = None
        extension_lib: 'extension_lib' = None

    def on_loading(self):
        global _
        _ = self.extension_lib.Program._
        self.extension_lib.Program.add_translation('zh_CN', {'Drawings': '绘图', 'Variable Selected': '选择的变量',
                                                             'No Variable': '尚未选择变量'})

    def on_load(self):
        drawings_toolbar: 'PMDrawingsToolBar' = self.widgets['PMDrawingsToolBar']
        drawings_toolbar.extension_lib = self.extension_lib
        self.drawings_toolbar = drawings_toolbar
        self.interface.drawing_item_double_clicked_signal = drawings_toolbar.drawing_item_double_clicked_signal

        self.interface.drawing_item_double_clicked_signal.connect(self.interface.on_clicked)
        self.interface.drawings_toolbar = drawings_toolbar

        self.extension_lib.Data.add_data_changed_callback(drawings_toolbar.on_data_modified)
        self.extension_lib.Signal.get_widgets_ready_signal().connect(self.bind_events)

    def bind_events(self):
        workspace_interface = self.extension_lib.get_interface('workspace_inspector')
        workspace_interface.add_select_data_callback(self.drawings_toolbar.on_data_selected)


class DrawingsInterface(BaseInterface):
    drawing_item_double_clicked_signal: 'pyqtSignal' = None
    drawings_toolbar: 'PMDrawingsToolBar' = None

    def on_clicked(self, name: str):
        pass
        # print('interface', name)

    def add_graph_button(self, name: str, text: str, icon_path: str, callback: Callable, hint: str = ''):
        """
        添加一个绘图按钮。name表示按钮的名称,text表示按钮的文字，icon_path表示按钮的图标路径，callback表示按钮的回调函数
        hint表示的就是按钮鼠标悬浮时候的提示文字。
        例如：
        extension_lib.get_interface('drawings_toolbar').add_graph_button('aaaaaa','hahahaahahah',
                                                                         ':/pyqt/source/images/lc_searchdialog.png',lambda :print('123123123'))
        """
        self.drawings_toolbar.add_toolbox_widget(name, text, icon_path, hint, refresh=True)


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    win1 = PMMenuToolPanel()

    win1.show()
    a = [1, 2, 3, 4]
    win1.variable = a

    sys.exit(app.exec())
