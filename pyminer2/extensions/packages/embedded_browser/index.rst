==============================================================================
内嵌浏览器
==============================================================================

.. toctree::
   :maxdepth: 2


   main.rst
   webbrowser.rst

.. automodule:: pyminer2.extensions.packages.embedded_browser
    :members:
    :undoc-members:
