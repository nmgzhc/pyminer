"""
此文件在IPython启动之时，输入到IPython的变量空间中。
它的作用是预先定义一些函数和魔术方法，定义与工作空间通信的方法。
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from IPython.core.magic import register_line_magic, register_cell_magic, register_line_cell_magic
import types
# from matgen import v_, h_, M_
from pyminer2.core import *

from pmgwidgets import BaseClient

__builtins = [__k for __k in globals().keys()]
__ip = get_ipython()

__var_name_list = []
__neglect_post_run = False  # 为True的时候，执行命令不触发回调。防止刷新造成无限反复。
__ip.neglect_post_run = False


def __filter_vars(__data: dict):
    """
    过滤掉可调用的变量，以防用户使用
    from numpy import *
    这一类操作，造成工作空间不堪重负。
    :param __data:
    :return:
    """
    __keys = []
    for __k in __data.keys():
        if callable(__data[__k]):
            __keys.append(__k)
    for __key in __keys:
        __data.pop(__key)
    return __data


def __refresh_vars():
    """
    刷新工作空间的变量。
    :return:
    """
    global __var_name_list
    __client = BaseClient()
    __data_message = {}
    __data = {
        __k: __v for __k, __v in globals().items() if
        __k not in __builtins and not __k.startswith('_') and not isinstance(
            __v,
            types.ModuleType)}
    __data = __filter_vars(__data)
    __name_list = list(__data.keys())
    __deleted_data_name = set(__var_name_list) - set(__name_list)
    __var_name_list = __name_list
    __existing_vars = __client.get_all_public_var_names()

    for __deleted_name in __deleted_data_name:
        # print(__existing_vars)
        if __deleted_name in __existing_vars:
            __client.delete_var(__deleted_name, provider='ipython')
    try:
        __client.set_var_dic(__data, 'ipython')
        __update_globals_from_workspace()
    except:
        import traceback
        traceback.print_exc()


def __update_globals_from_workspace():
    __client = BaseClient()
    globals().update(__client.get_all_vars())


def __delete_var(__var_name: str):
    """
    删除变量。删除变量时不向工作空间发信息（因为这个变量往往来自工作空间）
    :param __var_name:
    :return:
    """
    global __neglect_post_run, __var_name_list
    if __var_name in globals().keys():
        __unused = globals().pop(__var_name)
        __neglect_post_run = True
        __data = {__k: __v for __k, __v in globals().items()
                  if __k not in __builtins and not __k.startswith('_') and not isinstance(__v, types.ModuleType)}
        __var_name_list = list(__data.keys())


def __post_run_callback():
    """
    IPython代码执行后触发的回调函数
    :return:
    """
    __refresh_vars()


def __pre_run_callback():
    """
    IPython代码执行前触发的回调函数
    :return:
    """
    global __neglect_post_run, __var_name_list
    if not __neglect_post_run:
        __data = {__k: __v for __k, __v in globals().items()
                  if __k not in __builtins and not __k.startswith('_') and not isinstance(__v, types.ModuleType)}
        __var_name_list = list(__data.keys())

        __update_globals_from_workspace()
    else:
        __neglect_post_run = True


@register_line_cell_magic
def lcmagic(line, cell=None):
    """
    这是IPython魔术方法的一个例子，可以通过这个来找到例子。
    :param line:
    :param cell:
    :return:
    """
    if cell is None:
        print("Called as line magic")
        return line
    else:
        print("Called as cell magic")
        return line, cell


def __clear_all():
    """
    清除全部变量
    Returns:

    """
    global __var_name_list
    for __var_name in __var_name_list:
        globals().pop(__var_name)
    # __var_name_list = []


# print(cd)
__ip.events.register('post_run_cell', __post_run_callback)
__ip.events.register('pre_run_cell', __pre_run_callback)
