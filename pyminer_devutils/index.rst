===============
PyMiner开发工具
===============

.. toctree::
   :maxdepth: 2

   doc/index.rst

.. automodule:: pyminer_devutils
    :members:
    :undoc-members:

这个包主要用于承载PyMiner开发过程中所需要的一些功能，例如文档的编译等。

这个包存在的意义在于，在最终的发行版中，这个包不会被打包在内，这将减少与主程序无关的依赖，从而减小主程序的大小。
