"""
有无办法通过迭代器方式递归实现流程图的迭代仿真？
"""
import os
import sys
import time
from typing import List, Union, Tuple, Dict, TYPE_CHECKING, Callable
from pmgwidgets import PMGFlowContent, PMGPanelDialog

if TYPE_CHECKING:
    import pandas as pd


class PandasImport(PMGFlowContent):

    def __init__(self):
        super(PandasImport, self).__init__()
        self.input_args_labels = ['路径']
        self.output_ports_labels = ['数据集']  # , '检验']
        self.class_name = 'PandasImport'
        self.text = '读取Pandas数据集'
        self.icon_path = ''
        self.info = {'sampling_rate': 0.2}

    def process(self, path: str) -> List:
        """
        基础迭代器
        Args:
            *args:

        Returns:

        """

        assert isinstance(path, str)
        import pandas as pd
        ext = os.path.splitext(path)[1]

        if ext == '.csv':
            return [pd.read_csv(path)]
        elif ext == '.xls' or ext == '.xlsx':
            return [pd.read_excel(path)]
        else:
            raise ValueError('Cannot Read file of this extension: \'%s\'' % ext)

    def on_settings_requested(self, parent):
        """

        Args:
            parent:

        Returns:

        """
        return
        views = [('numberspin_ctrl', 'sampling_rate', '抽样比率', self.info['sampling_rate'], '', (0, 1), 0.0001)]
        dlg = PMGPanelDialog(parent=parent, views=views)

        dlg.setMinimumSize(600, 480)
        dlg.exec_()

        self.info['sampling_rate'] = dlg.panel.get_value()['sampling_rate']


if __name__ == '__main__':
    import sys

    from qtpy.QtWidgets import QApplication

    app = QApplication(sys.argv)

    # r.load_info({'gen_array': False, 'size': (1, 2, 3), 'type': 'normal'})
    # r.process()
    info = {'sampling_rate': 0.2}
    r = Iterator()
    r.load_info(info)
    r.on_settings_requested(None)
    print(r.info)
    sys.exit(app.exec_())
