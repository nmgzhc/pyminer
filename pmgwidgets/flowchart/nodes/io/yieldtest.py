import types
def a():
    for i in range(10):
        yield i


a1 = a()
print(type(a1),isinstance(a1,types.GeneratorType))
print(next(a1))
print(next(a1))
print(next(a1))
