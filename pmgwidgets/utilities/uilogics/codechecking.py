from typing import Tuple, Union, Any

ERROR_INVALID_TYPE = 'Parameter or Variable'
TYPE_RANGE = Tuple[Union[int, float], Union[int, float]]


def iter_isinstance(iter, elem_cls) -> bool:
    """
    检查可迭代变量是否为iter_type指定的类型。
    :param iter: 
    :param elem_cls: 
    :return: 
    """
    for item in iter:
        if not isinstance(item, elem_cls):
            return False
    return True


class ErrorReporter():
    @staticmethod
    def create_invalid_parameter_value_message(param_name: str, param_value: Any):
        return 'Parameter \'%s\' value %s is not allowed.' % (param_name, str(param_value))

    @staticmethod
    def create_invalid_parameter_type_message(param_name, param_type, expected_type) -> Any:
        return 'Parameter \'%s\' type %s is not allowed.Expected type is %s' % (
            param_name, str(param_type), str(expected_type))

    @staticmethod
    def create_invalid_variable_value_message(variable_name, variable_value):
        return 'Parameter \'%s\' value %s is not allowed.' % (variable_name, variable_value)
