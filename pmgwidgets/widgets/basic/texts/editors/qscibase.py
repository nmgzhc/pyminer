import os

from PyQt5.QtCore import Qt, pyqtSignal, QSize
from PyQt5.Qsci import QsciScintilla
from qtpy.QtGui import QImage, QColor


class PMGQsciWidget(QsciScintilla):
    MARKER_BREAKPOINT = 0
    MARKER_DEBUG_CURRENT_LINE = 1
    MARKER_RUN_CELL = 2
    MARKER_SET_VALUE = 3  # 设置值
    MARKER_RUN_FILE = 4
    signal_run = pyqtSignal(str)  # 可能是部分，也可能是全部。

    def __init__(self, parent=None):
        super().__init__(parent)
        root_path = os.path.dirname(__file__)

        self.markerDefine(QsciScintilla.Circle, self.MARKER_BREAKPOINT)
        self.markerDefine(QsciScintilla.RightArrow, self.MARKER_DEBUG_CURRENT_LINE)
        self.setMarkerBackgroundColor(QColor(219, 88, 96), 0)
        self.setMarkerBackgroundColor(QColor(88, 219, 96), 1)
        self.setMarkerBackgroundColor(QColor(88, 219, 96), 2)
        self.setMarkerBackgroundColor(QColor(88, 219, 96), 3)

        self.setMarkerForegroundColor(QColor(219, 88, 96), 0)
        self.setMarkerForegroundColor(QColor(88, 219, 96), 1)
        self.setMarkerForegroundColor(QColor(88, 219, 96), 2)
        self.setMarkerForegroundColor(QColor(88, 219, 96), 3)

        sym_run_cell = QImage(os.path.join(root_path, 'src', 'marker_run_cell.png')).scaled(QSize(16, 16))
        sym_set_value = QImage(os.path.join(root_path, 'src', 'marker_set_value.png')).scaled(QSize(16, 16))
        sym_run_file = QImage(os.path.join(root_path, 'src', 'marker_run_file.png')).scaled(QSize(16, 16))
        self.markerDefine(sym_run_cell, self.MARKER_RUN_CELL)
        self.markerDefine(sym_set_value, self.MARKER_SET_VALUE)
        self.markerDefine(sym_run_file, self.MARKER_RUN_FILE)
        # QsciScintilla.Arrow

    def _clear_default_shortcuts(self):
        """
        清除本身的快捷键。
        :return:
        """

        def clear_shortcut(keys):
            commands = self.standardCommands()
            command = commands.boundTo(keys)
            if command is not None:
                command.setKey(0)

        clear_shortcut(Qt.ControlModifier | Qt.Key_Slash)
        clear_shortcut(Qt.ControlModifier | Qt.AltModifier | Qt.Key_Right)
        clear_shortcut(Qt.ControlModifier | Qt.AltModifier | Qt.Key_Left)
