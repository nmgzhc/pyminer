<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="zh_CN" sourcelanguage="">
<context>
    <name>PMGTableViewer</name>
    <message>
        <location filename="../tableviews.py" line="283"/>
        <source>Slice</source>
        <translation>切片</translation>
    </message>
    <message>
        <location filename="../tableviews.py" line="284"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../tableviews.py" line="328"/>
        <source>Invalid Input</source>
        <translation>无效输入</translation>
    </message>
    <message>
        <location filename="../tableviews.py" line="321"/>
        <source>invalid character &quot;%s&quot; in slicing statement.</source>
        <translation>输入的语句含有无效字符 &quot;%s&quot;。</translation>
    </message>
</context>
</TS>
